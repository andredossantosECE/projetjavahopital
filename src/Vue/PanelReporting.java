package Vue;

import Controleur.Controleur;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.data.category.DefaultCategoryDataset;
import org.jfree.data.general.DefaultPieDataset;

import javax.swing.*;
import java.awt.*;
import java.util.HashMap;

/**
 * Class PanelReporting
 */
public class PanelReporting extends JPanel {

    private final Controleur controleur;

    private JPanel rootPanel;
    private JPanel panelContenu;

    private JPanel panel1;
    private JPanel panel2;
    private JPanel panel3;
    private JPanel panel4;

    private JPanel panelGraph1;
    private JPanel panelGraph2;
    private JPanel panelGraph3;
    private JPanel panelGraph4;

    private JButton btnRetourPageDeGarde;

    /**
     * Constructor
     * @param controleur - Controller
     */
    public PanelReporting(Controleur controleur) {
        this.controleur = controleur;

        this.panelContenu.setPreferredSize(new Dimension(1000, 500));
        this.btnRetourPageDeGarde.addActionListener(this.controleur);
        this.btnRetourPageDeGarde.setActionCommand("Retourner page de garde");

        this.add(rootPanel);
    }

    /**
     * Draws a PieChart for the number of beds occupied and available
     * @return - Chart Panel
     */
    public ChartPanel createPieChartByBedsAvailability() {
        DefaultPieDataset dataset = new DefaultPieDataset( );
        Integer totalAmount = this.controleur.getTotalAmountOfBedrooms();
        Integer amountOccupied = this.controleur.getAmountOfOccupiedBedrooms();
        Integer amountFree = totalAmount - amountOccupied;
        dataset.setValue( "Lits disponibles", amountFree);
        dataset.setValue( "Lits occuppés", amountOccupied);

        JFreeChart chart = ChartFactory.createPieChart3D(
                "Nombre de lits disponibles ("+amountFree.toString()+") / Nombre de lits occuppés ("+amountOccupied.toString()+")",
                dataset,
                true,
                true,
                false);
        return new ChartPanel(chart);
    }

    /**
     * Draws a PieChart for the number of nurses working during the day and night
     * @return - Chart Panel
     */
    public ChartPanel createPieChartByScheduleTurns() {
        DefaultPieDataset dataset = new DefaultPieDataset( );
        Integer amountDay = this.controleur.getAmountOfEmployesWorkingByDay();
        Integer amountNight = this.controleur.getAmountOfEmployesWorkingByNight();
        dataset.setValue( "Nombre d'infirmiers le jour", amountDay);
        dataset.setValue( "Nombre d'infirmiers la nuit", amountNight);

        JFreeChart chart = ChartFactory.createPieChart3D(
                "Infirmiers le jour ("+amountDay.toString()+") / Infirmiers la nuit ("+amountNight.toString()+")",
                dataset,
                true,
                true,
                false);
        return new ChartPanel(chart);
    }

    /**
     * Draws a BarChart with the number of patients and nurses per service
     * @return - Chart Panel
     */
    public ChartPanel createBarChartPatientAndInfirmierByService() {
        DefaultCategoryDataset dataset = new DefaultCategoryDataset();
        HashMap<String, Integer> amountOfInfirmiersByService = this.controleur.getAmountOfInfirmiersByService();
        HashMap<String, Integer> amountOfPatientsByService = this.controleur.getAmountOfPatientsByService();

        for (String key : amountOfInfirmiersByService.keySet()) {
            dataset.setValue(amountOfInfirmiersByService.get(key), "Infirmiers", key);
        }
        for (String key : amountOfPatientsByService.keySet()) {
            dataset.setValue(amountOfPatientsByService.get(key), "Patients", key);
        }

        JFreeChart barChart = ChartFactory.createBarChart(
                "Nombre de patients et infirmiers par service",
                "",
                "Nombre de personnes",
                dataset,
                PlotOrientation.VERTICAL,
                true, true, false);

        return new ChartPanel(barChart);
    }

    private void createUIComponents() {
        panelGraph1 = createPieChartByBedsAvailability();
        panelGraph2 = createPieChartByScheduleTurns();
        panelGraph3 = createBarChartPatientAndInfirmierByService();
    }

    private void $$$setupUI$$$() {
        createUIComponents();
    }
}
