package Vue;

import Controleur.Controleur;

import javax.swing.*;
import java.awt.*;

/**
 * Class PanelInscription
 */
public class PanelInscription extends JPanel {

    private final Controleur controleur;

    private JLabel labNom = new JLabel("Nom");
    private JTextField fieldNom = new JTextField();
    private JLabel labPrenom = new JLabel("Prénom");
    private JTextField fieldPrenom = new JTextField();
    private JLabel labTelephone = new JLabel("Telephone");
    private JTextField fieldTelephone = new JTextField();
    private JLabel labFonction = new JLabel("Fonction");
    private JComboBox<String> listFonction = new JComboBox<>(new String[]{"Infirmier", "Docteur"});
    private JLabel labIdentifiant = new JLabel("Identifiant");
    private JTextField fieldIdentifiant = new JTextField();
    private JLabel labMotDePasse = new JLabel("Mot de passe");
    private JPasswordField fieldMotDePasse = new JPasswordField();
    private JLabel labConfirmationMdp = new JLabel("Confirmation mot de passe");
    private JPasswordField fieldConfirmationMdp = new JPasswordField();
    private JButton btnRetour = new JButton("Retour");
    private JButton btnValider = new JButton("Valider");
    private String errorMessage = "";

    /**
     * Constructor
     * @param controleur - controller
     */
    public PanelInscription(Controleur controleur){
        this.controleur = controleur;

        // --- Layout
        GridBagLayout layout = new GridBagLayout();
        GridBagConstraints contrainte = new GridBagConstraints();
        this.setLayout(layout);

        // --- Ajout des éléments
        contrainte.insets = new Insets(5, 5, 5, 5);
        fieldNom.setPreferredSize(new Dimension(100,20));
        fieldPrenom.setPreferredSize(new Dimension(100,20));
        fieldTelephone.setPreferredSize(new Dimension(100,20));
        fieldIdentifiant.setPreferredSize(new Dimension(100, 20));
        fieldMotDePasse.setPreferredSize(new Dimension(100,20));
        fieldConfirmationMdp.setPreferredSize(new Dimension(100, 20));

        // Lab : nom
        contrainte.gridx = 0;
        contrainte.gridy = 0;
        contrainte.anchor = GridBagConstraints.LINE_START;
        this.add(labNom, contrainte);

        // Lab : prenom
        contrainte.gridy = 1;
        this.add(labPrenom, contrainte);

        // Lab : Telephone
        contrainte.gridy = 2;
        this.add(labTelephone, contrainte);

        // Lab : Fonction
        contrainte.gridy = 3;
        this.add(labFonction, contrainte);

        // Lab : Identifiant
        contrainte.gridy = 4;
        this.add(labIdentifiant, contrainte);

        // Lab : Mot de passe
        contrainte.gridy = 5;
        this.add(labMotDePasse, contrainte);

        // Lab : Confirmation mot de passe
        contrainte.gridy = 6;
        this.add(labConfirmationMdp, contrainte);

        // Bouton : Retour
        contrainte.gridy = 7;
        this.add(btnRetour, contrainte);

        // Field : Nom
        contrainte.gridx = 1;
        contrainte.gridy = 0;
        contrainte.anchor = contrainte.anchor = GridBagConstraints.LINE_END;
        this.add(fieldNom, contrainte);

        // Field : Prenom
        contrainte.gridy = 1;
        this.add(fieldPrenom, contrainte);

        // Field : Telephone
        contrainte.gridy = 2;
        this.add(fieldTelephone, contrainte);

        // List : Fonction
        contrainte.gridy = 3;
        this.add(listFonction, contrainte);

        // Field : Identifiannt
        contrainte.gridy = 4;
        this.add(fieldIdentifiant, contrainte);

        // Field : Mot de passe
        contrainte.gridy = 5;
        this.add(fieldMotDePasse, contrainte);

        // Field : Confirmation mot de passe
        contrainte.gridy = 6;
        this.add(fieldConfirmationMdp, contrainte);

        // Bouton : Valider
        contrainte.gridy = 7;
        this.add(btnValider, contrainte);

        btnRetour.addActionListener(controleur);
        btnRetour.setActionCommand("Retour vers login");

        btnValider.addActionListener(controleur);
        btnValider.setActionCommand("Valider inscription");
    }

    /**
     * Getter for fieldNom
     * @return - fieldNom
     */
    public JTextField getFieldNom() {
        return fieldNom;
    }

    /**
     * Setter for fieldNom
     * @param fieldNom - fieldNom
     */
    public void setFieldNom(JTextField fieldNom) {
        this.fieldNom = fieldNom;
    }

    /**
     * Getter for fieldPrenom
     * @return - fieldPrenom
     */
    public JTextField getFieldPrenom() {
        return fieldPrenom;
    }

    /**
     * Setter for fieldPrenom
     * @param fieldPrenom - fieldPrenom
     */
    public void setFieldPrenom(JTextField fieldPrenom) {
        this.fieldPrenom = fieldPrenom;
    }

    /**
     * Getter for fieldTelephone
     * @return - fieldTelephone
     */
    public JTextField getFieldTelephone() {
        return fieldTelephone;
    }

    /**
     * Setter for fieldTelephone
     * @param fieldTelephone - fieldTelephone
     */
    public void setFieldTelephone(JTextField fieldTelephone) {
        this.fieldTelephone = fieldTelephone;
    }

    /**
     * Getter for listFonction
     * @return - listFonction
     */
    public JComboBox<String> getListFonction() {
        return listFonction;
    }

    /**
     * Setter for listFonction
     * @param listFonction - listFonction
     */
    public void setListFonction(JComboBox<String> listFonction) {
        this.listFonction = listFonction;
    }

    /**
     * Getter for fieldIdentifiant
     * @return - fieldIdentifiant
     */
    public JTextField getFieldIdentifiant() {
        return fieldIdentifiant;
    }

    /**
     * Setter for fieldIdentifiant
     * @param fieldIdentifiant - fieldIdentifiant
     */
    public void setFieldIdentifiant(JTextField fieldIdentifiant) {
        this.fieldIdentifiant = fieldIdentifiant;
    }

    /**
     * Getter for fieldMotDePasse
     * @return - fieldMotDePasse
     */
    public JPasswordField getFieldMotDePasse() {
        return fieldMotDePasse;
    }

    /**
     * Setter for fieldMotDePasse
     * @param fieldMotDePasse - fieldMotDePasse
     */
    public void setFieldMotDePasse(JPasswordField fieldMotDePasse) {
        this.fieldMotDePasse = fieldMotDePasse;
    }

    /**
     * Getter for fieldConfirmationMdp
     * @return - fieldConfirmationMdp
     */
    public JPasswordField getFieldConfirmationMdp() {
        return fieldConfirmationMdp;
    }

    /**
     * Setter for fieldConfirmationMdp
     * @param fieldConfirmationMdp - fieldConfirmationMdp
     */
    public void setFieldConfirmationMdp(JPasswordField fieldConfirmationMdp) {
        this.fieldConfirmationMdp = fieldConfirmationMdp;
    }

    /**
     * Getter for errorMessage
     * @return - errorMessage
     */
    public String getErrorMessage() {
        return errorMessage;
    }

    /**
     * Setter for errorMessage
     * @param errorMessage - errorMessage
     */
    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    /**
     * Adder for errorMessage
     * @param errorMessage - errorMessage
     */
    public void addErrorMessage(String errorMessage) {
        this.errorMessage += errorMessage;
    }

    /**
     * Emptier for errorMessage
     */
    public void emptyErrorMessage() {
        this.errorMessage = "";
    }

    /**
     * Shows the errorMessage dialog
     */
    public void showErrorMessage() {
        String message;
        if (this.errorMessage.isEmpty()) { message = "Formulaire inscription non conforme!"; }
        else { message = this.errorMessage; }
        JOptionPane.showMessageDialog(new JFrame(), message, "Ups", JOptionPane.ERROR_MESSAGE);
    }
}
