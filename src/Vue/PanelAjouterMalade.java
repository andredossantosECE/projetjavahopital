package Vue;

import Controleur.Controleur;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * Class PanelAjouterMalade
 */
public class PanelAjouterMalade extends JFrame implements ActionListener {

    private final Controleur controleur;
    public ArrayList<String> codeServices = new ArrayList<>();
    public HashMap<Integer, ArrayList<Integer>> listeChambresLibres;
    public HashMap<Integer, HashMap<String, Object>> listeOfMyDoctors;
    public HashMap<Integer, HashMap<String, Object>> listeOfAllDoctors;
    public Integer noMalade = 0;
    public String selectedCodeService;
    public Integer selectedNoChambre;
    public Integer selectedNoLit;
    public String selectedNom;
    public String selectedPrenom;
    public String selectedTelephone;
    public boolean usingNewDoctor = true;

    private JPanel rootPanel;

    // ------------------------------------ Service Section

    private JPanel panelService;
    private JComboBox comboService;
    private JButton btnValiderService;

    // ------------------------------------ Chambre Section

    private JPanel panelChambreLit;
    private JComboBox comboNoLit;
    private JComboBox comboNoChambre;
    private JButton btnValiderLitEtChambre;

    // ------------------------------------ Patient info Check Section

    private JPanel panelInfosMalade;
    private JTextField fieldNomMalade;
    private JTextField fieldPrenomMalade;
    private JTextField fieldTelephoneMalade;
    private JButton btnValiderInfosMalade;

    // ------------------------------------ Extra patient info Section

    private JPanel panelMutuelleAdresse;
    public JTextField fieldMutuelle;
    public JTextField fieldAdresse;

    // ------------------------------------ Docteur Section

    private JPanel panelDocteur;
    public JComboBox comboDocteur;

    // ------------------------------------ Validate Section

    private JPanel panelEnregistrer;
    private JButton btnEnregister;

    /**
     * Constructor
     * @param controleur - Controller
     */
    public PanelAjouterMalade(Controleur controleur) {
        this.controleur = controleur;

        // --------------------------------------- Block all the fields while service not chosen yet

        this.comboNoChambre.setEditable(false);
        this.comboNoChambre.setEnabled(false);
        this.comboNoLit.setEditable(false);
        this.comboNoLit.setEnabled(false);

        this.fieldNomMalade.setEditable(false);
        this.fieldPrenomMalade.setEditable(false);
        this.fieldTelephoneMalade.setEditable(false);

        this.fieldMutuelle.setEditable(false);
        this.fieldAdresse.setEditable(false);

        this.comboDocteur.setEditable(false);
        this.comboDocteur.setEnabled(false);

        // ---------------------------------------- We fill the services combobox

        ArrayList<HashMap<String, Object>> allServices = this.controleur.getAllAvailableServices();
        for (HashMap<String, Object> serviceActuel : allServices) {
            this.codeServices.add(serviceActuel.get("code").toString());
            this.comboService.addItem(serviceActuel.get("nom").toString());
        }

        this.btnValiderService.addActionListener(this);
        this.btnValiderService.setActionCommand("Valider service");

        // ---------------------------------------- Choice of rooms section

        this.btnValiderLitEtChambre.addActionListener(this);
        this.btnValiderLitEtChambre.setActionCommand("Valider chambre et lit");
        this.btnValiderLitEtChambre.setEnabled(false);

        // ---------------------------------------- Check patient info section

        this.btnValiderInfosMalade.addActionListener(this);
        this.btnValiderInfosMalade.setActionCommand("Valider et rechercher patient");
        this.btnValiderInfosMalade.setEnabled(false);

        // ----------------------------------------- Save new patient section

        this.btnEnregister.addActionListener(this.controleur);
        this.btnEnregister.setActionCommand("Enregistrer nouveau patient");
        this.btnEnregister.setEnabled(false);

        // Construction of the JFrame
        this.setLocation(800, 200);
        this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        this.setSize(500, 600);
        this.setTitle("Ajouter nouveau patient");
        this.setContentPane(this.rootPanel);
        this.setVisible(true);
    }

    /**
     * Fills the ComboBox with the doctors (All or only those of the patient)
     * @param noMalade - Patient's number
     */
    public void fillComboDoctor(int noMalade) {
        if (comboDocteur.getItemCount() > 0) comboDocteur.removeAllItems();
        int cpt = 0;
        if (noMalade > 0) {
            this.usingNewDoctor = false;
            // Search for his current doctors that already treated him at least once
            ArrayList<HashMap<String, Object>> myDoctors = this.controleur.retrievePatientDoctor(noMalade);
            this.listeOfMyDoctors = new HashMap<>();
            for (HashMap<String, Object> doctor : myDoctors) {
                this.listeOfMyDoctors.put(cpt, doctor);
                this.comboDocteur.addItem(doctor.get("nom").toString() + " (" + doctor.get("specialite").toString() + ")");
                cpt++;
            }
        } else {
            this.usingNewDoctor = true;
            // Search for all the doctors on hospital
            ArrayList<HashMap<String, Object>> allDoctors = this.controleur.retieveAllDoctors();
            listeOfAllDoctors = new HashMap<>();
            for (HashMap<String, Object> doctor : allDoctors) {
                listeOfAllDoctors.put(cpt, doctor);
                comboDocteur.addItem(doctor.get("nom").toString() + " (" + doctor.get("specialite").toString() + ")");
                cpt++;
            }
        }
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        switch (e.getActionCommand()) {
            case "Valider service": {
                this.selectedCodeService = this.codeServices.get(this.comboService.getSelectedIndex());
                this.listeChambresLibres = this.controleur.getFreeChambresByCodeService(this.selectedCodeService);
                if (this.comboNoChambre.getItemCount() > 0) this.comboNoChambre.removeAllItems();
                for (Integer no_chambre : listeChambresLibres.keySet()) {
                    this.comboNoChambre.addItem(no_chambre);
                }
                this.comboNoChambre.setEnabled(true);
                this.comboNoChambre.addActionListener (e1 -> {
                    if (comboNoChambre.getItemCount() > 0) {
                        Integer selectedChambre = (Integer) comboNoChambre.getSelectedItem();
                        ArrayList<Integer> listeLitsCorrespondantes = listeChambresLibres.get(selectedChambre);
                        if (comboNoLit.getItemCount() > 0) comboNoLit.removeAllItems();
                        for (Integer lit : listeLitsCorrespondantes) {
                            comboNoLit.addItem(lit);
                        }
                        comboNoLit.setEnabled(true);
                        btnValiderLitEtChambre.setEnabled(true);
                    }
                });
                break;
            }
            case "Valider chambre et lit": {
                this.selectedNoChambre = (Integer) this.comboNoChambre.getSelectedItem();
                this.selectedNoLit = (Integer) this.comboNoLit.getSelectedItem();

                this.fieldNomMalade.setEditable(true);
                this.fieldPrenomMalade.setEditable(true);
                this.fieldTelephoneMalade.setEditable(true);
                this.btnValiderInfosMalade.setEnabled(true);
                break;
            }
            case "Valider et rechercher patient": {
                this.selectedNom = this.fieldNomMalade.getText();
                this.selectedPrenom = this.fieldPrenomMalade.getText();
                this.selectedTelephone = this.fieldTelephoneMalade.getText();
                if (this.selectedNom.isEmpty() || this.selectedPrenom.isEmpty() || this.selectedTelephone.isEmpty()) break;

                // Checks if the patient is already registered on our database or not
                this.noMalade = this.controleur.checkIfPatientExistsInHospitalData(this.selectedNom, this.selectedPrenom, this.selectedTelephone);
                if (noMalade == 0) {
                    fillComboDoctor(0);
                } else {
                    // Retrieve Mutuelle And Adresse Information from patient
                    HashMap<String, String> mutuelleAndAdresseMalade = this.controleur.getPatientsMutuelleAndAdresse(noMalade);
                    this.fieldMutuelle.setText(mutuelleAndAdresseMalade.get("mutuelle"));
                    this.fieldAdresse.setText(mutuelleAndAdresseMalade.get("adresse"));

                    // We fill the combobox of doctors with the patient's doctors
                    fillComboDoctor(noMalade);
                    this.comboDocteur.addItem("Autre");
                    this.comboDocteur.addActionListener (e1 -> {
                        if (comboDocteur.getSelectedItem().equals("Autre")) {
                            fillComboDoctor(0);
                        }
                    });
                }

                this.fieldMutuelle.setEditable(true);
                this.fieldAdresse.setEditable(true);
                this.comboDocteur.setEnabled(true);
                this.btnEnregister.setEnabled(true);
                break;
            }
            default:
                break;
        }
    }
}
