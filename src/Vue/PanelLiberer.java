package Vue;

import Controleur.Controleur;

import javax.swing.*;

/**
 * Class PanelLiberer
 */
public class PanelLiberer extends JFrame {

    private final Controleur controleur;

    private JPanel rootPanel;

    private JPanel panelInfoLiberation;
    private JTextField fieldNom;
    private JTextField fieldPrenom;
    private JTextField filedTelephone;

    private JPanel panelBtnValider;
    private JButton btnValiderLiberation;

    /**
     * Constructor
     * @param controleur - Controller
     */
    public PanelLiberer(Controleur controleur) {
        this.controleur = controleur;

        this.btnValiderLiberation.addActionListener(this.controleur);
        this.btnValiderLiberation.setActionCommand("Liberer malade");

        // Construction of the JFrame
        this.setLocation(800, 200);
        this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        this.setSize(500, 220);
        this.setTitle("Libérer un patient");
        this.setContentPane(this.rootPanel);
        this.setVisible(true);
    }

    /**
     * Getter for fieldNom
     * @return - fieldNom
     */
    public JTextField getFieldNom() {
        return fieldNom;
    }

    /**
     * Getter for fieldPrenom
     * @return - fieldPrenom
     */
    public JTextField getFieldPrenom() {
        return fieldPrenom;
    }

    /**
     * Getter for filedTelephone
     * @return - filedTelephone
     */
    public JTextField getFiledTelephone() {
        return filedTelephone;
    }
}
