package Vue;


import Controleur.Controleur;

import javax.swing.*;
import java.awt.*;
import java.awt.event.KeyEvent;

import static java.awt.GridBagConstraints.CENTER;

/**
 * Class PanelLogin
 */
public class PanelLogin extends JPanel {

    private final Controleur controleur;

    private JLabel identifiant = new JLabel("Identifiant");
    private JLabel motDePasse = new JLabel("Mot de passe");
    private JPasswordField champMotDePasse = new JPasswordField();
    private JTextField champIdentifiant = new JTextField();
    private JButton btnConnexion = new JButton("Connexion");
    private JButton btnInscription = new JButton("Inscription");

    /**
     * Constructor
     * @param controleur - Controller
     */
    public PanelLogin(Controleur controleur) {
        this.controleur = controleur;

        // --- Création du layout
        GridBagLayout layout = new GridBagLayout();
        GridBagConstraints contrainte = new GridBagConstraints();
        this.setLayout(layout);

        // --- Gestion des tailles
        champIdentifiant.setPreferredSize(new Dimension(100, 20));
        champMotDePasse.setPreferredSize(new Dimension(100, 20));

        // --- Ajout des éléments
        contrainte.gridx = 0;
        contrainte.gridy = 0;
        contrainte.insets = new Insets(10, 10, 10, 10);

        // Lab : Identifiant
        contrainte.gridx = 0;
        contrainte.gridy = 1;
        contrainte.insets = new Insets(5,5,5,5);
        contrainte.anchor = GridBagConstraints.LINE_START;
        this.add(identifiant, contrainte);

        // Field : Identifiant
        contrainte.gridx = 1;
        contrainte.gridy = 1;
        contrainte.insets = new Insets(5,5,5,5);
        contrainte.anchor = GridBagConstraints.LINE_END;
        this.add(champIdentifiant, contrainte);

        // Lab : Mdp
        contrainte.gridx = 0;
        contrainte.gridy = 2;
        contrainte.insets = new Insets(5,5,5,5);
        contrainte.anchor = GridBagConstraints.LINE_START;
        this.add(motDePasse, contrainte);

        // Field : Mdp
        contrainte.gridx = 1;
        contrainte.gridy = 2;
        contrainte.insets = new Insets(5,5,5,5);
        contrainte.anchor = GridBagConstraints.LINE_END;
        this.add(champMotDePasse, contrainte);

        // Btn : Inscription
        contrainte.gridx = 1;
        contrainte.gridy = 3;
        contrainte.insets = new Insets(5,5,5,5);
        contrainte.anchor = CENTER;
        this.add(btnInscription, contrainte);

        // Btn : Connexion
        contrainte.gridx = 0;
        contrainte.gridy = 3;
        contrainte.insets = new Insets(5,5,5,5);
        contrainte.anchor = CENTER;
        btnConnexion.setMnemonic(KeyEvent.VK_ENTER);
        this.add(btnConnexion, contrainte);

        btnConnexion.addActionListener(controleur);
        btnConnexion.setActionCommand("Se connecter");


        btnInscription.addActionListener(controleur);
        btnInscription.setActionCommand("S'inscrire");
    }

    /**
     * Getter for champMotDePasse
     * @return - champMotDePasse
     */
    public JPasswordField getChampMotDePasse() {
        return champMotDePasse;
    }

    /**
     * Getter for champIdentifiant
     * @return - champIdentifiant
     */
    public JTextField getChampIdentifiant() {
        return champIdentifiant;
    }
}
