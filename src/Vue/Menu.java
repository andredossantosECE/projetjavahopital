package Vue;

import Controleur.Controleur;

import javax.swing.*;

/**
 * Class Menu
 */
public class Menu extends JMenuBar{

    private final Controleur controleur;

    /**
     * Constructor
     * @param controleur - Controller
     */
    public Menu(Controleur controleur){
        this.controleur = controleur;

        // Items
        JMenu fichier = new JMenu("Fichier");

        JMenuItem monCompte = new JMenuItem("Mon Compte");
        monCompte.addActionListener(controleur);
        monCompte.setActionCommand("Acceder mon compte");
        fichier.add(monCompte);

        JMenuItem pageDeGarde = new JMenuItem("Page de Garde");
        pageDeGarde.addActionListener(controleur);
        pageDeGarde.setActionCommand("Retourner page de garde");
        fichier.add(pageDeGarde);

        JMenuItem pageReporting = new JMenuItem("Reporting");
        pageReporting.addActionListener(controleur);
        pageReporting.setActionCommand("Acceder reporting");
        fichier.add(pageReporting);

        JMenuItem deconnexion = new JMenuItem("Deconnéxion");
        deconnexion.addActionListener(controleur);
        deconnexion.setActionCommand("Retour vers login");
        fichier.add(deconnexion);

        JMenu patient = new JMenu("Patient");

        JMenuItem ajouterMalade = new JMenuItem("Ajouter patient");
        ajouterMalade.addActionListener(controleur);
        ajouterMalade.setActionCommand("Ouvrir popup nouveau malade");
        patient.add(ajouterMalade);

        JMenuItem libererMalade = new JMenuItem("Libérer patient");
        libererMalade.addActionListener(controleur);
        libererMalade.setActionCommand("Ouvrir popup liberer malade");
        patient.add(libererMalade);

        if (!this.controleur.getUserLogin().isValidated()) {
            monCompte.setEnabled(false);
            pageDeGarde.setEnabled(false);
            pageReporting.setEnabled(false);
            ajouterMalade.setEnabled(false);
            libererMalade.setEnabled(false);
        }

        this.add(fichier);
        this.add(patient);
    }
}
