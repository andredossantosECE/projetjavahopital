package Vue;

import Controleur.Controleur;

import javax.swing.*;

/**
 * Class FenetreMere (MotherFrame)
 */
public class FenetreMere extends JFrame {

    /**
     * Constructor
     */
    public FenetreMere(){
        super("ERP Hopital");
        this.setLocation(400, 200);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setContentPane(new JPanel());
        this.setVisible(true);
    }

    /**
     * Adapts the mother frame for the Login panel
     * @param panel - login panel
     */
    public void afficheLogin(JPanel panel){
        this.setSize(300,170);
        this.setTitle("Connexion");
        this.setContentPane(new JPanel());
        this.getContentPane().add(panel);
        this.setVisible(true);
    }

    /**
     * Adapts the mother frame for the Inscription panel
     * @param panel - Inscription panel
     */
    public void afficheInscription(JPanel panel){
        this.setSize(350, 300);
        this.setTitle("Inscription");
        this.setContentPane(new JPanel());
        this.getContentPane().add(panel);
        this.setVisible(true);
    }

    /**
     * Adapts the mother frame for the Main panel
     * @param controleur - Controller
     * @param panel - Main panel
     */
    public void affichePanelPrincipal(Controleur controleur, JPanel panel){
        Menu menu = new Menu(controleur);
        this.setJMenuBar(menu);
        this.setSize(1200, 700);
        this.setTitle("Hopital");
        this.setContentPane(new JPanel());
        this.getContentPane().add(panel);
        this.setVisible(true);
    }
}
