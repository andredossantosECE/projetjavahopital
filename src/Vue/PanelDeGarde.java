package Vue;

import Controleur.Controleur;

import javax.swing.*;
import java.awt.*;
import java.util.HashMap;

public class PanelDeGarde extends JPanel {

    private JPanel rootPanel;
    private final Controleur controleur;
    private boolean iHaveNoPatients = false;

    // ------------------------------------ Components 1st degree

    private JPanel panelHeader;                         // Header
    private JPanel interactiveButtonsPanel;             // Buttons for adding/deleting/reporting
    private JScrollPane scrollPaneBody;                 // Body with the tables

    // ------------------------------------ Components 2nd degree

    private JLabel labUsername;                         // Icon and username
    private JPanel panelGestion;                        // Panel for profile and hospital
    private JPanel panelUserData;                       // Panel info user

    private JButton rentrerNouveauMaladeButton;         // Button to add patient
    private JButton libererMaladeButton;                // Button to discharge patient
    private JButton reportingButton;                    // Button to access reporting

    private JPanel panelContenu;                        // Panel with all the body content

    // ------------------------------------ Components 3rd degree

    private JButton monCompteButton;                    // Button for managing profile
    private JButton monHopitalButton;                   // Button for managing Hospital

    private JLabel nomLabel;                            // Label "Nom :"
    private JLabel fieldNom;                            // Field for user surname
    private JLabel labPrenom;                           // Label "Prénom :"
    private JLabel fieldPrenom;                         // Field for user first name
    private JLabel labFonction;                         // Label "Fonction :"
    private JLabel fieldFonction;                       // Field for user function

    private JPanel panelMesPatients;                    // Panel with my patients
    private JLabel labMesPatients;                      // Label "Mes Patients :"
    private JPanel panelTousLesPatients;                // Panel with all patients
    private JLabel labTousLesPatients;                  // Label "Tous les patiensts :"

    private JScrollPane scrollPanelMesPatients;         // Scroll panel with own patients table
    private JTable tableMesPatients;                    // Table with own patients
    private JScrollPane scrollPanelTousLesPatients;     // Scroll panel with all patients table
    private JTable tableTousPatients;                   // Table with all patients

    public PanelDeGarde(Controleur controleur) {
        this.controleur = controleur;

        // ---------------------------------------------------------------- Header
        String username = this.controleur.getUserLogin().getUsername();
        String fonction = this.controleur.getUserLogin().getFonction();
        labUsername.setText(username);

        if (username.equals("admin")) {
            fieldNom.setText("Administrateur");
            fieldPrenom.setText("Supréme");
            fieldFonction.setText("Admin");
        } else {
            HashMap<String, Object> meEmployeInfo = this.controleur.getEmployeInfoCurrentUser(this.controleur.getUserLogin().getEmploye());
            fieldNom.setText(meEmployeInfo.get("nom").toString());
            fieldPrenom.setText(meEmployeInfo.get("prenom").toString());
            fieldFonction.setText(fonction);
        }

        if (!this.controleur.getUserLogin().isValidated()) {
            this.monCompteButton.setEnabled(false);
            this.monHopitalButton.setEnabled(false);
            this.rentrerNouveauMaladeButton.setEnabled(false);
            this.libererMaladeButton.setEnabled(false);
            this.reportingButton.setEnabled(false);
        }

        if (username.equals("admin")) this.monCompteButton.setText("Gérer les comptes");


        this.monCompteButton.addActionListener(this.controleur);
        this.monCompteButton.setActionCommand("Acceder mon compte");

        // TODO Later development
        this.monHopitalButton.setEnabled(false);

        // ---------------------------------------------------------------- Interactive buttons

        this.rentrerNouveauMaladeButton.addActionListener(this.controleur);
        this.rentrerNouveauMaladeButton.setActionCommand("Ouvrir popup nouveau malade");

        this.libererMaladeButton.addActionListener(this.controleur);
        this.libererMaladeButton.setActionCommand("Ouvrir popup liberer malade");

        this.reportingButton.addActionListener(this.controleur);
        this.reportingButton.setActionCommand("Acceder reporting");

        // ---------------------------------------------------------------- Tables

        // Check if the user is a Docteur, if not remove the docteur's patients table
        if (fonction == null) this.rootPanel.remove(this.panelContenu);
        else if (!fonction.equals("Docteur") || this.iHaveNoPatients) panelContenu.remove(panelMesPatients);

        this.panelMesPatients.setPreferredSize(new Dimension(1100, 250));
        this.panelTousLesPatients.setPreferredSize(new Dimension(1100, 250));
        this.panelContenu.setPreferredSize(new Dimension(1100, 425));
        this.scrollPaneBody.setPreferredSize(new Dimension(1100, 500));

        // We add the root panel to the JFrame
        this.add(this.rootPanel);
    }

    public JPanel getPanelContenu() {
        return panelContenu;
    }

    private void createUIComponents() {
        String fonction = this.controleur.getUserLogin().getFonction();
        if (fonction != null && this.controleur.getUserLogin().isValidated()) {
            String entetes[] = {"Prénom", "Nom", "Téléphone", "Mutuelle", "No Chambre", "No Lit"};
            if (fonction.equals("Docteur")) {
                Object[][] dataMesPatients = this.controleur.getMyCurrentPatientsHospitalised(this.controleur.getUserLogin().getEmploye());
                if (dataMesPatients == null) this.iHaveNoPatients = true;
                else tableMesPatients = new JTable(dataMesPatients, entetes);
            }
            Object[][] dataTousLesPatients = this.controleur.getAllCurrentPatientsHospitalised();
            tableTousPatients = new JTable(dataTousLesPatients, entetes);
        }
    }

    private void $$$setupUI$$$() {
        createUIComponents();
    }
}
