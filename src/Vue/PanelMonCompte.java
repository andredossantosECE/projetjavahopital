package Vue;

import Controleur.Controleur;

import javax.swing.*;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * Class PanelMonCompte
 */
public class PanelMonCompte extends JFrame {

    private Controleur controleur;

    public String selectedLoginToActivate;

    private JPanel rootPanel;
    private JPanel panelMonCompte;
    private JTextField fieldTelephone;
    private JTextField fieldPrenom;
    private JTextField fieldNom;
    private JButton btnModifierCompte;
    private JPanel panelGestionComptes;
    private JList listeLoginsNonActives;
    private JScrollPane scrollListeLoginsNonActives;

    public String hydratedNom;
    public String hydratedPrenom;
    public String hydratedTel;

    /**
     * Constructor
     * @param controleur - Controller
     */
    public PanelMonCompte(Controleur controleur) {
        this.controleur = controleur;

        if (this.controleur.getUserLogin().getUsername().equals("admin")) {
            this.panelMonCompte.setVisible(false);

            btnModifierCompte.addActionListener(controleur);
            btnModifierCompte.setActionCommand("Activer un compte");
            btnModifierCompte.setText("Activer le compte selectionné");

            this.setTitle("Gestion des comptes");
            this.setSize(400, 300);
        } else {
            this.panelGestionComptes.setVisible(false);

            // Retrieving of the employe info
            HashMap<String, Object> myInfo = this.controleur.getEmployeInfoCurrentUser(this.controleur.getUserLogin().getEmploye());

            hydratedNom = myInfo.get("nom").toString();
            hydratedPrenom = myInfo.get("prenom").toString();
            hydratedTel = myInfo.get("tel").toString();
            hydratedTel = hydratedTel.replaceAll(" ", "");

            this.fieldNom.setText(hydratedNom);
            this.fieldPrenom.setText(hydratedPrenom);
            this.fieldTelephone.setText(hydratedTel);

            btnModifierCompte.addActionListener(controleur);
            btnModifierCompte.setActionCommand("Modifier mon compte");

            this.setTitle("Mon compte");
            this.setSize(400, 200);
        }

        // Construction of the JFrame
        this.setLocation(800, 300);
        this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        this.setContentPane(this.rootPanel);
        this.setVisible(true);
    }

    /**
     * Getter for fieldTelephone
     * @return - fieldTelephone
     */
    public JTextField getFieldTelephone() {
        return fieldTelephone;
    }

    /**
     * Setter for fieldTelephone
     * @param fieldTelephone - fieldTelephone
     */
    public void setFieldTelephone(JTextField fieldTelephone) {
        this.fieldTelephone = fieldTelephone;
    }

    /**
     * Getter for fieldPrenom
     * @return - fieldPrenom
     */
    public JTextField getFieldPrenom() {
        return fieldPrenom;
    }

    /**
     * Setter for fieldPrenom
     * @param fieldPrenom - fieldPrenom
     */
    public void setFieldPrenom(JTextField fieldPrenom) {
        this.fieldPrenom = fieldPrenom;
    }

    /**
     * Getter for fieldNom
     * @return - fieldNom
     */
    public JTextField getFieldNom() {
        return fieldNom;
    }

    /**
     * Setter for fieldNom
     * @param fieldNom - fieldNom
     */
    public void setFieldNom(JTextField fieldNom) {
        this.fieldNom = fieldNom;
    }

    private void createUIComponents() {
        // TODO: place custom component creation code here
        if (this.controleur.getUserLogin().getUsername().equals("admin")) {
            DefaultListModel listModel = new DefaultListModel();
            ArrayList<String> loginsToActivate = this.controleur.getUnactivatedLoginsList();
            for (String login : loginsToActivate) {
                listModel.addElement(login);
            }
            this.listeLoginsNonActives = new JList(listModel);

            this.listeLoginsNonActives.addListSelectionListener(e -> {
                if (!e.getValueIsAdjusting()) {
                    selectedLoginToActivate = listeLoginsNonActives.getSelectedValue().toString();
                    System.out.println(selectedLoginToActivate);
                }
            });

        } else {
            this.listeLoginsNonActives = new JList();
        }
    }

    private void $$$setupUI$$$() {
        createUIComponents();
    }
}
