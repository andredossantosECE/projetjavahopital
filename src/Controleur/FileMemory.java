package Controleur;

import Modele.*;

import java.io.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

/**
 * Class FileMemory for the binary files functionality
 */
public class FileMemory implements TraitementDonnees {

//    private static final String FILE_MEMORY_PATH = "donnees/FileMemory/";
    private static final String FILE_MEMORY_PATH = "data/";

    private ArrayList<Object> collectionChambres;
    private ArrayList<Object> collectionDocteurs;
    private ArrayList<Object> collectionEmploye;
    private ArrayList<Object> collectionHospitalisation;
    private ArrayList<Object> collectionInfirmier;
    private ArrayList<Object> collectionMalade;
    private ArrayList<Object> collectionService;
    private ArrayList<Object> collectionSoigne;
    private ArrayList<Object> collectionLogin;

    private HashMap<String, ArrayList<Object>> allCollections;

    /**
     *  Constructor
     */
    public FileMemory() {
        this.allCollections = new HashMap<>();
        this.collectionChambres = new ArrayList<>();
        this.allCollections.put("Chambre", collectionChambres);
        this.collectionDocteurs = new ArrayList<>();
        this.allCollections.put("Docteur", collectionDocteurs);
        this.collectionEmploye = new ArrayList<>();
        this.allCollections.put("Employe", collectionEmploye);
        this.collectionHospitalisation = new ArrayList<>();
        this.allCollections.put("Hospitalisation", collectionHospitalisation);
        this.collectionInfirmier = new ArrayList<>();
        this.allCollections.put("Infirmier", collectionInfirmier);
        this.collectionMalade = new ArrayList<>();
        this.allCollections.put("Malade", collectionMalade);
        this.collectionService = new ArrayList<>();
        this.allCollections.put("Service", collectionService);
        this.collectionSoigne = new ArrayList<>();
        this.allCollections.put("Soigne", collectionSoigne);
        this.collectionLogin = new ArrayList<>();
        this.allCollections.put("Login", collectionLogin);
    }

    // ---------------------------------------- Getters/Setters

    /**
     * Getter for collectionChambres
     * @return collectionChambres
     */
    public ArrayList<Object> getCollectionChambres() {
        return collectionChambres;
    }

    /**
     * Setter for collectionChambres
     * @param collectionChambres - Chambres collection
     */
    public void setCollectionChambres(ArrayList<Object> collectionChambres) {
        this.collectionChambres = collectionChambres;
    }

    /**
     * Getter for collectionDocteurs
     * @return collectionDocteurs
     */
    public ArrayList<Object> getCollectionDocteurs() {
        return collectionDocteurs;
    }

    /**
     * Setter for collectionDocteurs
     * @param collectionDocteurs - Doctors collection
     */
    public void setCollectionDocteurs(ArrayList<Object> collectionDocteurs) {
        this.collectionDocteurs = collectionDocteurs;
    }

    /**
     * Getter for collectionEmploye
     * @return - collectionEmploye
     */
    public ArrayList<Object> getCollectionEmploye() {
        return collectionEmploye;
    }

    /**
     * Setter for collectionEmploye
     * @param collectionEmploye - Employee collection
     */
    public void setCollectionEmploye(ArrayList<Object> collectionEmploye) {
        this.collectionEmploye = collectionEmploye;
    }

    /**
     * Getter for collectionHospitalisation
     * @return - collectionHospitalisation
     */
    public ArrayList<Object> getCollectionHospitalisation() {
        return collectionHospitalisation;
    }

    /**
     * Setter for collectionHospitalisation
     * @param collectionHospitalisation - Hospitalisations collection
     */
    public void setCollectionHospitalisation(ArrayList<Object> collectionHospitalisation) {
        this.collectionHospitalisation = collectionHospitalisation;
    }

    /**
     * Getter for collectionInfirmier
     * @return - collectionInfirmier
     */
    public ArrayList<Object> getCollectionInfirmier() {
        return collectionInfirmier;
    }

    /**
     * Setter for collectionInfirmier
     * @param collectionInfirmier - Nurses collection
     */
    public void setCollectionInfirmier(ArrayList<Object> collectionInfirmier) {
        this.collectionInfirmier = collectionInfirmier;
    }

    /**
     * Getter for collectionMalade
     * @return - collectionMalade
     */
    public ArrayList<Object> getCollectionMalade() {
        return collectionMalade;
    }

    /**
     * Setter for collectionMalade
     * @param collectionMalade - Patients collection
     */
    public void setCollectionMalade(ArrayList<Object> collectionMalade) {
        this.collectionMalade = collectionMalade;
    }

    /**
     * Getter for collectionService
     * @return - collectionService
     */
    public ArrayList<Object> getCollectionService() {
        return collectionService;
    }

    /**
     * Setter for collectionService
     * @param collectionService - Services collection
     */
    public void setCollectionService(ArrayList<Object> collectionService) {
        this.collectionService = collectionService;
    }

    /**
     * Getter for collectionSoigne
     * @return - collectionSoigne
     */
    public ArrayList<Object> getCollectionSoigne() {
        return collectionSoigne;
    }

    /**
     * Setter for collectionSoigne
     * @param collectionSoigne - Treatements collection
     */
    public void setCollectionSoigne(ArrayList<Object> collectionSoigne) {
        this.collectionSoigne = collectionSoigne;
    }

    /**
     * Getter for collectionLogin
     * @return - collectionLogin
     */
    public ArrayList<Object> getCollectionLogin() {
        return collectionLogin;
    }

    /**
     * Setter for collectionLogin
     * @param collectionLogin - Login's collection
     */
    public void setCollectionLogin(ArrayList<Object> collectionLogin) {
        this.collectionLogin = collectionLogin;
    }

    // ---------------------------------------- Override Methods

    /**
     * Creates a new user with the given data
     * @param nom - Surname of the user
     * @param prenom - First name of the user
     * @param tel - Telephone number of the user
     * @param fonction - Function/Job of the user
     * @param username - Username for the new user
     * @param password - Password for the new user
     * @return - Returns true if user was successfully created, and false otherwise
     */
    @Override
    public boolean inscription(String nom, String prenom, String tel, String fonction, String username, String password) {
        Employe newEmploye = new Employe(this.getNewId("Employe"), nom, prenom, tel);
        int employeNo = newEmploye.getNumero();
        this.collectionEmploye.add(newEmploye);

        if (fonction.equals("Infirmier")) {
            this.collectionInfirmier.add(new Infirmier(employeNo, "JOUR"));
        } else if (fonction.equals("Docteur")) {
            this.collectionDocteurs.add(new Docteur(employeNo));
        }

        Login newLogin = new Login(this.getNewId("Login"), username, password, employeNo, fonction);
        this.collectionLogin.add(newLogin);

        this.saveAllObjects();
        return true;
    }

    /**
     * Verifies if the username and password exist and correspond to each other
     * @param  username - User's username
     * @param password - User's password
     * @return Returns true if the username and password are correct, and false otherwise
     */
    @Override
    public boolean authentification(String username, String password) {
        for (Object object : collectionLogin) {
            Login login = (Login) object;
            if (login.getUsername().equals(username) && login.getPassword().equals(password)) {
                return true;
            }
        }
        return false;
    }

    /**
     * Generic SELECT function for the database
     * @param tableName - Corresponds to the table name
     * @param columnNames - Corresponds to the columns for the WHERE conditions
     * @param values - Corresponds to the values for the WHERE conditions
     * @return Returns the results of the selection
     */
    @Override
    public ArrayList<HashMap<String, Object>> selection(String tableName, String[] columnNames, String[] values) throws NoSuchFieldException {
        ArrayList<HashMap<String, Object>> results = new ArrayList<>();
        ArrayList<Object> table = this.allCollections.get(tableName);
        String[] columnStructure = this.getObjectColumnStructure(tableName);
        for (int i = 0; i < table.size(); i++) {
            Object rowObject = table.get(i);
            Object[] rowValues = new Object[columnNames.length];
            for (int j = 0; j < columnNames.length; j++ ) {
                if (tableName.equals("Chambre")) {
                    Chambre chambre = (Chambre) rowObject;
                    Object value = chambre.getFieldUniversally(columnNames[j]);
                    rowValues[j] = value;
                } else if (tableName.equals("Docteur")) {
                    Docteur docteur = (Docteur) rowObject;
                    Object value = docteur.getFieldUniversally(columnNames[j]);
                    rowValues[j] = value;
                } else if (tableName.equals("Employe")) {
                    Employe employe = (Employe) rowObject;
                    Object value = employe.getFieldUniversally(columnNames[j]);
                    rowValues[j] = value;
                } else if (tableName.equals("Hospitalisation")) {
                    Hospitalisation hospitalisation = (Hospitalisation) rowObject;
                    Object value = hospitalisation.getFieldUniversally(columnNames[j]);
                    rowValues[j] = value;
                } else if (tableName.equals("Infirmier")) {
                    Infirmier infirmier = (Infirmier) rowObject;
                    Object value = infirmier.getFieldUniversally(columnNames[j]);
                    rowValues[j] = value;
                } else if (tableName.equals("Login")) {
                    Login login = (Login) rowObject;
                    Object value = login.getFieldUniversally(columnNames[j]);
                    rowValues[j] = value;
                } else if (tableName.equals("Malade")) {
                    Malade malade = (Malade) rowObject;
                    Object value = malade.getFieldUniversally(columnNames[j]);
                    rowValues[j] = value;
                } else if (tableName.equals("Service")) {
                    Service service = (Service) rowObject;
                    Object value = service.getFieldUniversally(columnNames[j]);
                    rowValues[j] = value;
                } else if (tableName.equals("Soigne")) {
                    Soigne soigne = (Soigne) rowObject;
                    Object value = soigne.getFieldUniversally(columnNames[j]);
                    rowValues[j] = value;
                }
            }
            HashMap<String, Object> currentRow = new HashMap<>();
            boolean conditionsAreMet = true;
            for (int k = 0; k < rowValues.length; k++) {
                if (!rowValues[k].toString().equals(values[k])) {
                    conditionsAreMet = false;
                    break;
                } else {
                    currentRow.put(columnNames[k], rowValues[k]);
                }
            }
            if (conditionsAreMet) {
                results.add(currentRow);
            }
        }
        return null;
    }

    /**
     * Generic INSERT function for the database
     * @param tableName - Corresponds to the table name
     * @param columnNames - Corresponds to the columns of the insertion
     * @param newValues - Corresponds to the new values for the insertion
     * @return Returns true if the insertions were successful, and false otherwise
     */
    @Override
    public boolean insertion(String tableName, String[] columnNames, String[] newValues) {
        return false;
    }

    /**
     * Generic UPDATE function for the database
     * @param tableName - Corresponds to the table name
     * @param columnNames - Corresponds to the columns that need to be modified
     * @param newValues - Corresponds to the new values that replace the old ones
     * @param conditionColumns - Corresponds to the columns for the WHERE conditions
     * @param conditionValues - Corresponds to the values for the WHERE conditions
     * @return Returns true if the modifications were successful, and false otherwise
     */
    @Override
    public boolean modification(String tableName, String[] columnNames, String[] newValues, String[] conditionColumns, String[] conditionValues) {
        return false;
    }

    /**
     * Deletes a tuple or object from the database/file memory
     * @param tableName - Corresponds to the table name
     * @param conditionColumns - Corresponds to the columns for the WHERE conditions
     * @param conditionValues - Corresponds to the values for the WHERE conditions
     * @return Returns true if the deletes were successful, and false otherwise
     */
    @Override
    public boolean suppression(String tableName, String[] conditionColumns, String[] conditionValues) {
        return false;
    }

    // ---------------------------------------- Static Methods

    /**
     * Deletes all the '' inside the String
     * @param text - Text that needs to be treated
     * @return Returns a treated string with no ''
     */
    private static String parseGoodString(String text) {
        if (text.equals("null")) return null;
        if (text.startsWith("'") && text.endsWith("'")) {
            text =  text.substring(1, text.length() - 1);
        }
        return text;
    }

    /**
     * Replaces "null" with "0" in order to avoid problems with Integer.parser
     * @param number - String with a number as text or null
     * @return - Returns "0" if text equals "null"
     */
    private static String checkIfIntegerIsNull(String number) {
        if (number.equals("null")) return "0";
        else return number;
    }

    // ---------------------------------------- Essential Methods

    /**
     * Responsible for open a file
     * @param path - Path to open the file
     * @return - Returns a object of type File
     */
    private File openFile(String path) {
        File file = new File(path);
        if (file.exists()) {
            return file;
        }
        else return null;
    }

    /**
     * Method responsible for interpreting the rows and transform them into cells,
     * by dividing them by putting in consideration the "'" that are inside the text elements
     * that shouldn't be split. Ex: 'I'm a good guy!' = ["I'm a good guy"]
     * and not ["I"]["m a good guy"]
     * @param row - Row of data that needs to be treated and split into cells
     * @return - Returns a spliced row as a String[]
     */
    private String[] splitLineIntoCellules(String row) {
        String currentCellule = "";
        ArrayList<String> ensembleCellules = new ArrayList<>();
        boolean insideAString = false;
        for (String character: row.split("")) {
            if (character.equals("'")) {
                insideAString = !insideAString;
                currentCellule += character;
            } else if (character.equals(",")) {
                if (insideAString) {
                    currentCellule += character;
                } else {
                    ensembleCellules.add(currentCellule);
                    currentCellule = "";
                }
            } else {
                if (!insideAString && character.equals(" ")) continue;
                else currentCellule += character;
            }
        }
        ensembleCellules.add(currentCellule);
        String[] cellules = new String[ensembleCellules.size()];
        Iterator iter = ensembleCellules.iterator();
        int indice = 0;
        while (iter.hasNext()) {
            cellules[indice] = iter.next().toString();
            indice++;
        }
        return cellules;
    }

    /**
     * Retrieves the data from a file with the given name, and returns the data as an ArrayList of String[],
     * and each String[] corresponds to a row, and the content inside the String[] the cells
     * @param filename - Name of the file to treat
     * @return - Returns the data of the file
     * @throws IOException -Thrown when a problem occurs when manipulating the file
     */
    public ArrayList<String[]> getFileContent(String filename) throws IOException {
        File file = this.openFile(FileMemory.FILE_MEMORY_PATH + filename);
        if (file != null) {
            FileReader fr = new FileReader(file);
            BufferedReader bfr = new BufferedReader(fr);
            ArrayList<String[]> contenu = new ArrayList<>();
            String row;
            while ((row = bfr.readLine()) != null) {
                row = row.replaceFirst("\\(", "");
                row = row.replace("),", "");
                String[] cellules = splitLineIntoCellules(row);
                contenu.add(cellules);
            }
            fr.close();
            return contenu;
        }
        return null;
    }

    /**
     * Saves inside a binary file of the given name the object passed as parameter
     * @param object - ArrayList with the data to be saved
     * @param fileName - Name of the file for saving the data
     * @throws Exception - Thrown when a problem occurs when manipulating the file
     */
    public void save(ArrayList<Object> object, String fileName) throws Exception {
        FileOutputStream fos = new FileOutputStream(FileMemory.FILE_MEMORY_PATH + fileName);
        ObjectOutputStream oos = new ObjectOutputStream(fos);
        oos.writeObject(object);
        oos.flush();
        oos.close();
    }

    /**
     * Reads the binary file with the name given in parameter and returns the object with all the data
     * @param fileName - Name of the file for reading the data
     * @return object - Data loaded (ArrayList)
     * @throws Exception - Thrown when a problem occurs when manipulating the file
     */
    public ArrayList<Object> load(String fileName) throws Exception {
        FileInputStream fis = new FileInputStream(FileMemory.FILE_MEMORY_PATH + fileName);
        ObjectInputStream ois = new ObjectInputStream(fis);
        ArrayList<Object> object = (ArrayList<Object>) ois.readObject();
        ois.close();
        return object;
    }

    /**
     * Method responsible for retrieving the TXT data into ArrayLists
     */
    public void retrieveDataFromTxt() {
        try {
            collectionChambres = this.parseChambreTXTtoArrayList("Chambre.txt");
            collectionDocteurs = this.parseDocteurTXTtoArrayList("Docteur.txt");
            collectionEmploye = this.parseEmployeTXTtoArrayList("Employe.txt");
            collectionHospitalisation = this.parseHospitalisationTXTtoArrayList("Hospitalisation.txt");
            collectionInfirmier = this.parseInfirmierTXTtoArrayList("Infirmier.txt");
            collectionMalade = this.parseMaladeTXTtoArrayList("Malade.txt");
            collectionService = this.parseServiceTXTtoArrayList("Service.txt");
            collectionSoigne = this.parseSoigneTXTtoArrayList("Soigne.txt");
            collectionLogin = this.parseLoginTXTtoArrayList("Login.txt");
        } catch (IOException e) {
            e.printStackTrace(System.err);
        }
    }

    /**
     * Method responsible for filling the Controller collections with their data from the binary files
     */
    public void hydrateAllObjects() {
        try {
            collectionChambres = this.load("Chambre.data");
            collectionDocteurs = this.load("Docteur.data");
            collectionEmploye = this.load("Employe.data");
            collectionHospitalisation = this.load("Hospitalisation.data");
            collectionInfirmier = this.load("Infirmier.data");
            collectionMalade = this.load("Malade.data");
            collectionService = this.load("Service.data");
            collectionSoigne = this.load("Soigne.data");
            collectionLogin = this.load("Login.data");
        } catch (Exception e) {
            e.printStackTrace(System.err);
        }
    }

    /**
     * Method responsible for saving the Controller collections data into binary files
     */
    public void saveAllObjects() {
        try {
            this.save(collectionChambres, "Chambre.data");
            this.save(collectionDocteurs, "Docteur.data");
            this.save(collectionEmploye, "Employe.data");
            this.save(collectionHospitalisation, "Hospitalisation.data");
            this.save(collectionInfirmier, "Infirmier.data");
            this.save(collectionMalade, "Malade.data");
            this.save(collectionService, "Service.data");
            this.save(collectionSoigne, "Soigne.data");
            this.save(collectionLogin, "Login.data");
        } catch (Exception e) {
            e.printStackTrace(System.err);
        }
    }

    /**
     * Converts the TXT content into Binary Objects
     * @param fileName - Name of the file
     * @return - Returns an ArrayList filled with Chambres
     * @throws IOException - Thrown when a problem occurs when manipulating the file
     */
    public ArrayList<Object> parseChambreTXTtoArrayList(String fileName) throws IOException {
        ArrayList<Object> chambresRepository = new ArrayList<>();
        ArrayList<String[]> chambresUntreated = getFileContent(fileName);
        for (String[] line : chambresUntreated) {
            String[] columnNames = Chambre.getColumnNames();
            if (line.length == columnNames.length) {
                HashMap<String, String> tupple = new HashMap<>();
                for (int i = 0; i < line.length; i++) {
                    tupple.put(columnNames[i], line[i]);
                }
                chambresRepository.add(new Chambre(
                        Integer.parseInt(tupple.get("no_chambre")),
                        parseGoodString(tupple.get("code_service")),
                        Integer.parseInt(tupple.get("surveillant")),
                        Integer.parseInt(tupple.get("nb_lits"))
                ));
            }
        }
        return chambresRepository;
    }

    /**
     * Converts the TXT content into Binary Objects
     * @param fileName - Name of the file
     * @return - Returns an ArrayList filled with Docteurs
     * @throws IOException - Thrown when a problem occurs when manipulating the file
     */
    public ArrayList<Object> parseDocteurTXTtoArrayList(String fileName) throws IOException {
        ArrayList<Object> docteursRepository = new ArrayList<>();
        ArrayList<String[]> docteursUntreated = getFileContent(fileName);
        for (String[] line : docteursUntreated) {
            String[] columnNames = Docteur.getColumnNames();
            if (line.length == columnNames.length) {
                HashMap<String, String> tupple = new HashMap<>();
                for (int i = 0; i < line.length; i++) {
                    tupple.put(columnNames[i], line[i]);
                }
                docteursRepository.add(new Docteur(
                        Integer.parseInt(tupple.get("numero")),
                        parseGoodString(tupple.get("specialite"))
                ));
            }
        }
        return docteursRepository;
    }

    /**
     * Converts the TXT content into Binary Objects
     * @param fileName - Name of the file
     * @return - Returns an ArrayList filled with Employes
     * @throws IOException - Thrown when a problem occurs when manipulating the file
     */
    public ArrayList<Object> parseEmployeTXTtoArrayList(String fileName) throws IOException {
        ArrayList<Object> employesRepository = new ArrayList<>();
        ArrayList<String[]> employesUntreated = getFileContent(fileName);
        for (String[] line : employesUntreated) {
            String[] columnNames = Employe.getColumnNames();
            if (line.length == columnNames.length) {
                HashMap<String, String> tupple = new HashMap<>();
                for (int i = 0; i < line.length; i++) {
                    tupple.put(columnNames[i], line[i]);
                }
                employesRepository.add(new Employe(
                        Integer.parseInt(tupple.get("numero")),
                        parseGoodString(tupple.get("nom")),
                        parseGoodString(tupple.get("prenom")),
                        parseGoodString(tupple.get("tel")),
                        parseGoodString(tupple.get("adresse"))
                ));
            }
        }
        return employesRepository;
    }

    /**
     * Converts the TXT content into Binary Objects
     * @param fileName - Name of the file
     * @return - Returns an ArrayList filled with Hospitalisations
     * @throws IOException - Thrown when a problem occurs when manipulating the file
     */
    public ArrayList<Object> parseHospitalisationTXTtoArrayList(String fileName) throws IOException {
        ArrayList<Object> hospitalisationsRepository = new ArrayList<>();
        ArrayList<String[]> hospitalisationsUntreated = getFileContent(fileName);
        for (String[] line : hospitalisationsUntreated) {
            String[] columnNames = Hospitalisation.getColumnNames();
            if (line.length == columnNames.length) {
                HashMap<String, String> tupple = new HashMap<>();
                for (int i = 0; i < line.length; i++) {
                    tupple.put(columnNames[i], line[i]);
                }
                hospitalisationsRepository.add(new Hospitalisation(
                        Integer.parseInt(tupple.get("no_malade")),
                        parseGoodString(tupple.get("code_service")),
                        Integer.parseInt(tupple.get("no_chambre")),
                        Integer.parseInt(tupple.get("lit"))
                ));
            }
        }
        return hospitalisationsRepository;
    }

    /**
     * Converts the TXT content into Binary Objects
     * @param fileName - Name of the file
     * @return - Returns an ArrayList filled with Infirmiers
     * @throws IOException - Thrown when a problem occurs when manipulating the file
     */
    public ArrayList<Object> parseInfirmierTXTtoArrayList(String fileName) throws IOException {
        ArrayList<Object> infirmiersRepository = new ArrayList<>();
        ArrayList<String[]> infirmiersUntreated = getFileContent(fileName);
        for (String[] line : infirmiersUntreated) {
            String[] columnNames = Infirmier.getColumnNames();
            if (line.length == columnNames.length) {
                HashMap<String, String> tupple = new HashMap<>();
                for (int i = 0; i < line.length; i++) {
                    tupple.put(columnNames[i], line[i]);
                }
                infirmiersRepository.add(new Infirmier(
                        Integer.parseInt(tupple.get("numero")),
                        parseGoodString(tupple.get("code_service")),
                        parseGoodString(tupple.get("rotation")),
                        Float.parseFloat(checkIfIntegerIsNull(tupple.get("salaire")))
                ));
            }
        }
        return infirmiersRepository;
    }

    /**
     * Converts the TXT content into Binary Objects
     * @param fileName - Name of the file
     * @return - Returns an ArrayList filled with Malades
     * @throws IOException - Thrown when a problem occurs when manipulating the file
     */
    public ArrayList<Object> parseMaladeTXTtoArrayList(String fileName) throws IOException {
        ArrayList<Object> maladesRepository = new ArrayList<>();
        ArrayList<String[]> maladesUntreated = getFileContent(fileName);
        for (String[] line : maladesUntreated) {
            String[] columnNames = Malade.getColumnNames();
            if (line.length == columnNames.length) {
                HashMap<String, String> tupple = new HashMap<>();
                for (int i = 0; i < line.length; i++) {
                    tupple.put(columnNames[i], line[i]);
                }
                maladesRepository.add(new Malade(
                        Integer.parseInt(tupple.get("numero")),
                        parseGoodString(tupple.get("nom")),
                        parseGoodString(tupple.get("prenom")),
                        parseGoodString(tupple.get("adresse")),
                        parseGoodString(tupple.get("tel")),
                        parseGoodString(tupple.get("mutuelle"))
                ));
            }
        }
        return maladesRepository;
    }

    /**
     * Converts the TXT content into Binary Objects
     * @param fileName - Name of the file
     * @return - Returns an ArrayList filled with Services
     * @throws IOException - Thrown when a problem occurs when manipulating the file
     */
    public ArrayList<Object> parseServiceTXTtoArrayList(String fileName) throws IOException {
        ArrayList<Object> servicesRepository = new ArrayList<>();
        ArrayList<String[]> servicesUntreated = getFileContent(fileName);
        for (String[] line : servicesUntreated) {
            String[] columnNames = Service.getColumnNames();
            if (line.length == columnNames.length) {
                HashMap<String, String> tupple = new HashMap<>();
                for (int i = 0; i < line.length; i++) {
                    tupple.put(columnNames[i], line[i]);
                }
                servicesRepository.add(new Service(
                        parseGoodString(tupple.get("code")),
                        parseGoodString(tupple.get("nom")),
                        parseGoodString(tupple.get("batiment")),
                        Integer.parseInt(tupple.get("directeur"))
                ));
            }
        }
        return servicesRepository;
    }

    /**
     * Converts the TXT content into Binary Objects
     * @param fileName - Name of the file
     * @return - Returns an ArrayList filled with Soignes
     * @throws IOException - Thrown when a problem occurs when manipulating the file
     */
    public ArrayList<Object> parseSoigneTXTtoArrayList(String fileName) throws IOException {
        ArrayList<Object> soignesRepository = new ArrayList<>();
        ArrayList<String[]> soignesUntreated = getFileContent(fileName);
        for (String[] line : soignesUntreated) {
            String[] columnNames = Soigne.getColumnNames();
            if (line.length == columnNames.length) {
                HashMap<String, String> tupple = new HashMap<>();
                for (int i = 0; i < line.length; i++) {
                    tupple.put(columnNames[i], line[i]);
                }
                soignesRepository.add(new Soigne(
                        Integer.parseInt(tupple.get("no_docteur")),
                        Integer.parseInt(tupple.get("no_malade"))
                ));
            }
        }
        return soignesRepository;
    }

    /**
     * Converts the TXT content into Binary Objects
     * @param fileName - Name of the file
     * @return - Returns an ArrayList filled with Logins
     * @throws IOException - Thrown when a problem occurs when manipulating the file
     */
    public ArrayList<Object> parseLoginTXTtoArrayList(String fileName) throws IOException {
        ArrayList<Object> loginsRepository = new ArrayList<>();
        ArrayList<String[]> loginsUntreated = getFileContent(fileName);
        for (String[] line : loginsUntreated) {
            String[] columnNames = Login.getColumnNames();
            if (line.length == columnNames.length) {
                HashMap<String, String> tupple = new HashMap<>();
                for (int i = 0; i < line.length; i++) {
                    tupple.put(columnNames[i], line[i]);
                }
                loginsRepository.add(new Login(
                        Integer.parseInt(tupple.get("idLogin")),
                        parseGoodString(tupple.get("username")),
                        parseGoodString(tupple.get("password")),
                        Integer.parseInt(checkIfIntegerIsNull(tupple.get("employe"))),
                        parseGoodString(tupple.get("fonction")),
                        tupple.get("validated").equals("true")
                ));
            }
        }
        return loginsRepository;
    }

    // ---------------------------------------- Core methods

    /**
     * Returns the list of the column names of the current table
     * @param tableName - The table name (collection in this situation)
     * @return - Returns a String[]
     */
    public String[] getObjectColumnStructure(String tableName) {
        switch (tableName) {
            case "Chambre": return Chambre.getColumnNames();
            case "Docteur": return Docteur.getColumnNames();
            case "Employe": return Employe.getColumnNames();
            case "Hospitalisation": return Hospitalisation.getColumnNames();
            case "Infirmier": return Infirmier.getColumnNames();
            case "Login": return Login.getColumnNames();
            case "Malade": return Malade.getColumnNames();
            case "Service": return Service.getColumnNames();
            case "Soigne": return Soigne.getColumnNames();
            default: return new String[] {};
        }
    }

    /**
     * Searches for the biggest "id" (primary key) and returns it as an int
     * @param tableName - Name of the table
     * @return - Returns the last id
     */
    private int getLastIdFromCollection(String tableName) {
        int max = 0;
        switch (tableName) {
            case "Chambre": {
                for (Object object : this.collectionChambres) {
                    Chambre chambre = (Chambre) object;
                    if (chambre.getNo_chambre() > max) max = chambre.getNo_chambre();
                }
                break;
            }
            case "Docteur": {
                for (Object object : this.collectionDocteurs) {
                    Docteur docteur = (Docteur) object;
                    if (docteur.getNumero() > max) max = docteur.getNumero();
                }
                break;
            }
            case "Employe": {
                for (Object object : this.collectionEmploye) {
                    Employe employe = (Employe) object;
                    if (employe.getNumero() > max) max = employe.getNumero();
                }
                break;
            }
            case "Hospitalisation": {
                for (Object object : this.collectionHospitalisation) {
                    Hospitalisation hospitalisation = (Hospitalisation) object;
                    if (hospitalisation.getNo_malade() > max) max = hospitalisation.getNo_malade();
                }
                break;
            }
            case "Infirmier": {
                for (Object object : this.collectionInfirmier) {
                    Infirmier infirmier = (Infirmier) object;
                    if (infirmier.getNumero() > max) max = infirmier.getNumero();
                }
                break;
            }
            case "Login": {
                for (Object object : this.collectionLogin) {
                    Login login = (Login) object;
                    if (login.getIdLogin() > max) max = login.getIdLogin();
                }
                break;
            }
            case "Malade": {
                for (Object object : this.collectionMalade) {
                    Malade malade = (Malade) object;
                    if (malade.getNumero() > max) max = malade.getNumero();
                }
                break;
            }
            default: {
                break;
            }
        }
        return max;
    }

    /**
     * Retrieves the biggest id of the table and increments it
     * @param tableName - Name of the table
     * @return - Returns an incremented id for insertions
     */
    public int getNewId(String tableName) {
        return (this.getLastIdFromCollection(tableName) + 1);
    }

    /**
     * Checks the database/collection for the Employe with the number given and returns that same Employe
     * @param numero - Unique number of an Employe
     * @return - Returns an Employe
     */
    public Employe getEmployeByNumero(int numero) {
        for (Object ob : collectionEmploye) {
            Employe employe = (Employe) ob;
            if (employe.getNumero() == numero) {
                return employe;
            }
        }
        return null;
    }

    /**
     * Checks the database/collection for the Malade with the number given and returns that same Malade
     * @param numero - Unique number of a Malade
     * @return - Returns a Malade
     */
    public Malade getMaladeByNumero(int numero) {
        for (Object ob : collectionMalade) {
            Malade malade = (Malade) ob;
            if (malade.getNumero() == numero) {
                return malade;
            }
        }
        return null;
    }

    /**
     * Registers a new hospitalisation and adds the patient to the database/file system if it is not in it already
     * @param no_malade - Corresponds to the unique number for the patient
     * @param nomMalade - Surname of the patient
     * @param prenomMalade - First name of the patient
     * @param telMalade - Telephone number of the patient
     * @param mutuelleMalade - Health Care insurance
     * @param adresseMalade - Patient's current address
     * @param no_Docteur - The number of doctor assigned to the patient
     * @param code_service - The service in which the patient is being treated
     * @param no_chambre - The assigned room for the patient
     * @param lit - The number of the bed for the patient
     */
    public void registerNewPatient(int no_malade, String nomMalade, String prenomMalade, String telMalade, String mutuelleMalade, String adresseMalade, int no_Docteur, String code_service, int no_chambre, int lit) {
        if (no_malade == 0) {
            // insertion malade
            Malade malade = new Malade(this.getNewId("Malade"), nomMalade, prenomMalade, adresseMalade, telMalade, mutuelleMalade);
            this.collectionMalade.add(malade);
            no_malade = malade.getNumero();
        } else {
            // update malade
            for (int i = 0; i < this.collectionMalade.size(); i++) {
                Malade malade = (Malade) this.collectionMalade.get(i);
                if (malade.getNumero() == no_malade) {
                    malade.setMutuelle(mutuelleMalade);
                    malade.setAdresse(adresseMalade);
                    this.collectionMalade.set(i, malade);
                }
            }
        }

        // Soigne
        boolean isInside = false;
        for (Object o : this.collectionSoigne) {
            Soigne soigne = (Soigne) o;
            if (soigne.getNo_malade() == no_malade && soigne.getNo_docteur() == no_Docteur) {
                isInside = true;
                break;
            }
        }
        if (!isInside) this.collectionSoigne.add(new Soigne(no_Docteur, no_malade));

        // Hospitalisation
        this.collectionHospitalisation.add(new Hospitalisation(no_malade, code_service, no_chambre, lit));

        this.saveAllObjects();
    }

    /**
     * Removes patient from being hospitalised (doesn't delete the Dossier of the patient)
     * @param nom - Surname of the patient to be set free
     * @param prenom - First name of the patient to be set free
     * @param tel - Telephone number of the patient to be set free
     */
    public void libererPatient(String nom, String prenom, String tel) {
        for (Object o : this.collectionMalade) {
            Malade malade = (Malade) o;
            if (malade.getNom().equals(nom) && malade.getPrenom().equals(prenom) && malade.getTel().equals(tel)) {
                ArrayList<Object> foundHospitalisations = new ArrayList<>();
                ArrayList<Object> foundSoigne = new ArrayList<>();
                for (Object o1 : this.collectionHospitalisation) {
                    Hospitalisation hospitalisation = (Hospitalisation) o1;
                    if (hospitalisation.getNo_malade() == malade.getNumero()) {
                        foundHospitalisations.add(hospitalisation);
                    }
                }
                for (Object o2 : this.collectionSoigne) {
                    Soigne soigne = (Soigne) o2;
                    if (soigne.getNo_malade() == malade.getNumero()) {
                        foundSoigne.add(soigne);
                    }
                }
                this.collectionHospitalisation.removeAll(foundHospitalisations);
                this.collectionSoigne.removeAll(foundSoigne);
            }
        }
        this.saveAllObjects();
    }

    /**
     * Retrieves the amount of bedrooms
     * @return - Returns the amount of bedrooms
     */
    public int getTotalAmountOfBedrooms() {
        int amount = 0;
        for (Object o : this.collectionChambres) {
            Chambre chambre = (Chambre) o;
            amount += chambre.getNb_lits();
        }
        return amount;
    }

     /**
     * Retrieves the amount of occupied bedrooms
     * @return - Returns the amount of bedrooms occupied
     */
     public int getAmountOfOccupiedBedrooms() {
        return this.collectionHospitalisation.size();
     }

    /**
     * Retrieves the amount of staff personnel working during the day
     * @return - Returns a precise amount
     */
    public int getAmountOfEmployesWorkingByDay() {
        int amount = 0;
        for (Object o : this.collectionInfirmier) {
            Infirmier infirmier = (Infirmier) o;
            if (infirmier.getRotation().equals("JOUR")) amount++;
        }
        return amount;
    }

    /**
     * Retrieves the amount of staff personnel working during the night
     * @return - Returns a precise amount
     */
    public int getAmountOfEmployesWorkingByNight() {
        int amount = 0;
        for (Object o : this.collectionInfirmier) {
            Infirmier infirmier = (Infirmier) o;
            if (infirmier.getRotation().equals("NUIT")) amount++;
        }
        return amount;
    }

    /**
     * Retrieves the amount of nurses for each service
     * @return - Returns a list indexed by the service
     */
    public HashMap<String, Integer> getAmountOfInfirmiersByService() {
        HashMap<String, Integer> savedServices = new HashMap<>();
        for (Object o : this.collectionInfirmier) {
            Infirmier infirmier = (Infirmier) o;
            if (infirmier.getCode_service() == null) continue;
            if (savedServices.containsKey(infirmier.getCode_service())) {
                savedServices.put(infirmier.getCode_service(), savedServices.get(infirmier.getCode_service())+1);
            } else {
                savedServices.put(infirmier.getCode_service(), 1);
            }
        }
        return savedServices;
    }

    /**
     * Retrieves the amount of hospitalised patients for each service
     * @return - Returns a list indexed by the service
     */
    public HashMap<String, Integer> getAmountOfPatientsByService() {
        HashMap<String, Integer> savedServices = new HashMap<>();
        for (Object o : this.collectionHospitalisation) {
            Hospitalisation hospitalisation = (Hospitalisation) o;
            if (savedServices.containsKey(hospitalisation.getCode_service())) {
                savedServices.put(hospitalisation.getCode_service(), savedServices.get(hospitalisation.getCode_service())+1);
            } else {
                savedServices.put(hospitalisation.getCode_service(), 1);
            }
        }
        return savedServices;
    }
}
