package Controleur;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

public interface TraitementDonnees {

    /**
     * Creates a new user with the given data
     * @param nom - Surname of the user
     * @param prenom - First name of the user
     * @param tel - Telephone number of the user
     * @param fonction - Function/Job of the user
     * @param username - Username for the new user
     * @param password - Password for the new user
     * @return - Returns true if user was successfully created, and false otherwise
     */
    public boolean inscription(String nom, String prenom, String tel, String fonction, String username, String password);

    /**
     * Verifies if the username and password exist and correspond to each other
     * @param  username - User's username
     * @param password - User's password
     * @throws IOException - Throws exception on file manipulation
     * @return Returns true if the username and password are correct, and false otherwise
     */
    public boolean authentification(String username, String password) throws IOException;

    /**
     * Generic SELECT function for the database
     * @param tableName - Corresponds to the table name
     * @param columnNames - Corresponds to the columns for the WHERE conditions
     * @param values - Corresponds to the values for the WHERE conditions
     * @throws NoSuchFieldException - Throws error
     * @return Returns the results of the selection
     */
    public ArrayList<HashMap<String, Object>> selection(String tableName, String[] columnNames, String[] values) throws NoSuchFieldException;

    /**
     * Generic INSERT function for the database
     * @param tableName - Corresponds to the table name
     * @param columnNames - Corresponds to the columns of the insertion
     * @param newValues - Corresponds to the new values for the insertion
     * @return Returns true if the insertions were successful, and false otherwise
     */
    public boolean insertion(String tableName, String[] columnNames, String[] newValues);

    /**
     * Generic UPDATE function for the database
     * @param tableName - Corresponds to the table name
     * @param columnNames - Corresponds to the columns that need to be modified
     * @param newValues - Corresponds to the new values that replace the old ones
     * @param conditionColumns - Corresponds to the columns for the WHERE conditions
     * @param conditionValues - Corresponds to the values for the WHERE conditions
     * @return Returns true if the modifications were successful, and false otherwise
     */
    public boolean modification(String tableName, String[] columnNames, String[] newValues, String[] conditionColumns, String[] conditionValues);

    /**
     * Deletes a tuple or object from the database/file memory
     * @param tableName - Corresponds to the table name
     * @param conditionColumns - Corresponds to the columns for the WHERE conditions
     * @param conditionValues - Corresponds to the values for the WHERE conditions
     * @return Returns true if the deletes were successful, and false otherwise
     */
    public boolean suppression(String tableName, String[] conditionColumns, String[] conditionValues);
}
