package Controleur;

import Modele.*;
import Vue.*;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * Controller's class with the main method (start point)
 * The heart of the application
 */
public class Controleur implements ActionListener {

    private boolean RESTART_FILEMEMORY = false;

    // ---------------------------------------- Connexion/FileMemory

    private Connexion connexion;
    private FileMemory fileMemory;

    // ---------------------------------------- Logged User

    private Login userLogin;

    // ---------------------------------------- Vues

    private static FenetreMere fenetreMere;
    private PanelLogin panelLogin;
    private PanelInscription panelInscription;
    private PanelDeGarde panelDeGarde;
    private PanelMonCompte panelMonCompte;
    private PanelAjouterMalade panelAjouterMalade;
    private PanelLiberer panelLiberer;
    private PanelReporting panelReporting;

    // ---------------------------------------- Controleur

    /**
     * Constructor
     * @param restart - if true then file memory is restarted to its initial state
     */
    private Controleur(boolean restart) {
        // connexion bdd
        connexion = new Connexion();
        //hydrate les fichiers si methode "Data-filesystem"
        if (connexion.getMethod().equals("Data-filesystem")) {
            fileMemory = new FileMemory();
            // converts the .txt on .data (only needed for hydrate the data files)
            if (restart) {
                fileMemory.retrieveDataFromTxt();
                fileMemory.saveAllObjects();
            } else fileMemory.hydrateAllObjects();
        }
        // initialise la fenetre mêre
        fenetreMere = new FenetreMere();
        // initialise le panel login
        initialisePanelLogin();
    }

    // ---------------------------------------- Getters / Setters

    /**
     * Getter for the logged Login object
     * @return - userLogin
     */
    public Login getUserLogin() {
        return this.userLogin;
    }

    // ---------------------------------------- Vues

    /**
     * Initialises the Login panel
     */
    public void initialisePanelLogin() {
        panelLogin = new PanelLogin(this);
        fenetreMere.afficheLogin(panelLogin);
    }

    /**
     * Initialises the Inscription panel
     */
    public void initialisePanelInscription() {
        panelInscription = new PanelInscription(this);
        fenetreMere.afficheInscription(panelInscription);
    }

    /**
     * Initialises the HomePage panel
     */
    public void initialisePanelDeGarde() {
        panelDeGarde = new PanelDeGarde(this);
        fenetreMere.affichePanelPrincipal(this, panelDeGarde);
    }

    /**
     * Initialises the Profile popup
     */
    public void initialisePopupMonCompte() {
        panelMonCompte = new PanelMonCompte(this);
    }

    /**
     * Initialises the Add new Patient popup
     */
    public void initialisePopupAjouterMalade() {
        panelAjouterMalade = new PanelAjouterMalade(this);
    }

    /**
     * Initialises the release patient popup
     */
    public void initialisePopupLibererMalade() {
        panelLiberer = new PanelLiberer(this);
    }

    /**
     * Initialises the Reporting panel
     */
    public void initialisePanelReporting() {
        panelReporting = new PanelReporting(this);
        fenetreMere.affichePanelPrincipal(this, panelReporting);
    }

    // ---------------------------------------- Essential methods

    /**
     * Static method that verifies if the string given is composed with only letters (accents allowed)
     * @param msg - The message to be checked
     * @return - Returns true if the message given is valid, and false otherwise
     */
    public static boolean isOnlyLetters(String msg) {
        return !msg.equals("") && msg.matches("^[a-zA-ZÀ-ÿ ]*$");
    }

    /**
     * Static method that verifies if the string given is composed with only numbers
     * @param msg - The message to be checked
     * @return - Returns true if the message given is valid, and false otherwise
     */
    public static boolean isOnlyNumbers(String msg) {
        return !msg.equals("") && msg.matches("^[0-9]{10}$");
    }

    /**
     * Static method that checks if the string given doesn't have any whitespaces
     * @param msg - The message to be checked
     * @return - Returns true if the message is not composed with whitespaces, and false otherwise
     */
    public static boolean noWhitespaces(String msg) {
        return msg.length() == msg.trim().length();
    }

    /**
     * Adds a space after every pair of numbers to the telephone number
     * @param tel - Telephone number to be treated
     * @return - Returns something like this "06 01 02 03 04"
     */
    public static String telephoneBeautify(String tel) {
        // we add some whitespaces after every pair of numbers
        if (tel.length() == 10) {
            String tempTel = "";
            for (int i = 0; i < tel.length(); i++) {
                tempTel += tel.toCharArray()[i];
                if (!(i%2 == 0) && i != tel.length()-1) tempTel += " ";
            }
            tel = tempTel;
        }
        return tel;
    }

    // ---------------------------------------- Core methods

    /**
     * Verifies the integrity of the inscription form data
     * @return - Returns true if the form is correctly filled, or false otherwise
     */
    public boolean checkCredentials() {
        String username = panelLogin.getChampIdentifiant().getText();
        String password = String.valueOf(panelLogin.getChampMotDePasse().getPassword());
        boolean validLogin = false;
        if (connexion.getMethod().equals("MySQL-database")) {
            validLogin = connexion.authentification(username, password);
            if (validLogin) {
                ArrayList<HashMap<String, Object>> results = connexion.selection("Login", new String[] {"username"}, new String[] {username});
                HashMap<String, Object> line = results.get(0);
                int employeNumber = 0;
                if (line.get("employe") != null) employeNumber = (int) line.get("employe");
                userLogin = new Login(
                        (int) line.get("idLogin"),
                        (String) line.get("username"),
                        (String) line.get("password"),
                        employeNumber,
                        (String) line.get("fonction"),
                        (boolean) line.get("validated")
                );
            }
        } else if (connexion.getMethod().equals("Data-filesystem")) {
            validLogin = fileMemory.authentification(username, password);
            if (validLogin) {
                for (Object ob : fileMemory.getCollectionLogin()) {
                    Login login = (Login) ob;
                    if (login.getUsername().equals(username) && login.getPassword().equals(password)) {
                        this.userLogin = new Login(login.getIdLogin(), login.getUsername(), login.getPassword(), login.getEmploye(), login.getFonction(), login.isValidated());
                    }
                }
            }
        }
        return validLogin;
    }

    /**
     * Checks if the login's username and password are correct
     * @return - Returns true if the login is correct, false otherwise
     */
    public boolean valdidateInscription() {
        boolean valid = true;

        // We set the error message to empty
        this.panelInscription.emptyErrorMessage();

        String  prenom = this.panelInscription.getFieldPrenom().getText(),
                nom = this.panelInscription.getFieldNom().getText(),
                telephone = this.panelInscription.getFieldTelephone().getText(),
                mdp = String.valueOf(this.panelInscription.getFieldMotDePasse().getPassword()),
                confirmationMdp = String.valueOf(this.panelInscription.getFieldConfirmationMdp().getPassword());

        // Verfication of nom and prenom
        if (!isOnlyLetters(prenom)) {
            this.panelInscription.addErrorMessage(" Prenom doit contenir que des lettres.");
            valid = false;
        }
        if (!isOnlyLetters(nom)) {
            this.panelInscription.addErrorMessage(" Nom doit contenir que des lettres.");
            valid = false;
        }
        // Verification of telephone number
        if (!isOnlyNumbers(telephone)) {
            this.panelInscription.addErrorMessage(" Telephone doit contenir que 10 chiffres.");
            valid = false;
        }

        // Verfication of passwords and confirmation password
        if (!(mdp.equals(confirmationMdp) && noWhitespaces(mdp) && noWhitespaces(confirmationMdp))) {
            this.panelInscription.addErrorMessage(" Mots de passes ne coincident pas ou possédent des espaces.");
            valid = false;
        }

        return valid;
    }

    /**
     * Retrieves the employee data of the logged user
     * @param numero - Employee number associated to the user
     * @return - Returns a list with the user's employee info
     */
    public HashMap<String, Object> getEmployeInfoCurrentUser(int numero) {
        HashMap<String, Object> meEmploye = new HashMap<>();
        Employe employe = null;
        if (connexion.getMethod().equals("MySQL-database")) {
            ArrayList<HashMap<String, Object>> results = connexion.selection("Employe", new String[] {"numero"}, new String[] {Integer.toString(numero)});
            employe = new Employe((int) results.get(0).get("numero"), (String) results.get(0).get("nom"), (String) results.get(0).get("prenom"), telephoneBeautify((String) results.get(0).get("tel")), (String) results.get(0).get("adresse"));
        } else if (connexion.getMethod().equals("Data-filesystem")) {
            employe = fileMemory.getEmployeByNumero(numero);
        }
        meEmploye.put("numero", numero);
        meEmploye.put("nom", employe.getNom());
        meEmploye.put("prenom", employe.getPrenom());
        meEmploye.put("tel", employe.getTel());
        meEmploye.put("adresse", employe.getAdresse());
        return meEmploye;
    }

    /**
     * Returns a prepared type of list for the table with all the patients
     * @return - Returns a two dimensions table
     */
    public Object[][] getAllCurrentPatientsHospitalised() {
        Object[][] results = null;
        if (connexion.getMethod().equals("MySQL-database")) {
            ArrayList<HashMap<String, Object>> resultsHospitalisations = connexion.selection("Hospitalisation", new String[]{}, new String[]{});
            int numberOfRows = resultsHospitalisations.size();
            results = new Object[numberOfRows][6];
            for (int i = 0; i < numberOfRows; i++) {
                Integer no_malade = (Integer) resultsHospitalisations.get(i).get("no_malade");
                ArrayList<HashMap<String, Object>> resultsMalade = connexion.selection("Malade", new String[] {"numero"}, new String[] {Integer.toString(no_malade)});
                results[i][0] = resultsMalade.get(0).get("prenom").toString();
                results[i][1] = resultsMalade.get(0).get("nom").toString();
                results[i][2] = telephoneBeautify(resultsMalade.get(0).get("tel").toString());
                results[i][3] = resultsMalade.get(0).get("mutuelle").toString();
                results[i][4] = Integer.toString((Integer) resultsHospitalisations.get(i).get("no_chambre"));
                results[i][5] = Integer.toString((Integer) resultsHospitalisations.get(i).get("lit"));
            }
        } else if (connexion.getMethod().equals("Data-filesystem")) {
            ArrayList<Object> resultsHospitalisations = fileMemory.getCollectionHospitalisation();
            int numberOfRows = resultsHospitalisations.size();
            results = new Object[numberOfRows][6];
            for (int i = 0; i < numberOfRows; i++) {
                Hospitalisation hospitalisationCourante = (Hospitalisation) resultsHospitalisations.get(i);
                Malade malade = fileMemory.getMaladeByNumero(hospitalisationCourante.getNo_malade());
                results[i][0] = malade.getPrenom();
                results[i][1] = malade.getNom();
                results[i][2] = telephoneBeautify(malade.getTel());
                results[i][3] = malade.getMutuelle();
                results[i][4] = hospitalisationCourante.getNo_malade();
                results[i][5] = hospitalisationCourante.getLit();
            }
        }
        return results;
    }

    /**
     * Returns a prepared type of list for the table of the logged doctor's patients
     * @param numero - Doctor's number
     * @return - Returns a two dimensions table
     */
    public Object[][] getMyCurrentPatientsHospitalised(int numero) {
        Object[][] results = null;
        ArrayList<Object[]> myPatients = new ArrayList<>();
        ArrayList<Integer> listeNumerosMesPatients = new ArrayList<>();
        if (connexion.getMethod().equals("MySQL-database")) {
            ArrayList<HashMap<String, Object>> resultsMyPatients = connexion.selection("Soigne", new String[] {"no_docteur"}, new String[] {Integer.toString(numero)});
            int numberOfPatientsIHave = resultsMyPatients.size();
            if (numberOfPatientsIHave == 0) return null;
            for (int i = 0; i < numberOfPatientsIHave; i++) {
                listeNumerosMesPatients.add((Integer) resultsMyPatients.get(i).get("no_malade"));
            }
            ArrayList<HashMap<String, Object>> resultsHospitalisations = connexion.selection("Hospitalisation", new String[]{}, new String[]{});
            int numberOfRows = resultsHospitalisations.size();
            for (int i = 0; i < numberOfRows; i++) {
                Integer no_malade = (Integer) resultsHospitalisations.get(i).get("no_malade");
                if (listeNumerosMesPatients.contains(no_malade)) {
                    Object[] line = new Object[6];
                    ArrayList<HashMap<String, Object>> resultsMalade = connexion.selection("Malade", new String[] {"numero"}, new String[] {Integer.toString(no_malade)});
                    line[0] = resultsMalade.get(0).get("prenom");
                    line[1] = resultsMalade.get(0).get("nom");
                    line[2] = resultsMalade.get(0).get("tel");
                    line[3] = resultsMalade.get(0).get("mutuelle");
                    line[4] = Integer.toString((Integer) resultsHospitalisations.get(i).get("no_chambre"));
                    line[5] = Integer.toString((Integer) resultsHospitalisations.get(i).get("lit"));
                    myPatients.add(line);
                }
            }
        } else if (connexion.getMethod().equals("Data-filesystem")) {
            ArrayList<Object> collectionSoigne = fileMemory.getCollectionSoigne();
            for (Object ob : collectionSoigne) {
                Soigne soigneCourant = (Soigne)  ob;
                if (soigneCourant.getNo_docteur() == numero) {
                    listeNumerosMesPatients.add(soigneCourant.getNo_malade());
                }
            }
            if (listeNumerosMesPatients.size() == 0) return null;
            ArrayList<Object> resultsHospitalisations = fileMemory.getCollectionHospitalisation();
            int numberOfRows = resultsHospitalisations.size();
            for (int i = 0; i < numberOfRows; i++) {
                Hospitalisation hospitalisationCourante = (Hospitalisation) resultsHospitalisations.get(i);
                Malade maladeCourant = fileMemory.getMaladeByNumero(hospitalisationCourante.getNo_malade());
                if (listeNumerosMesPatients.contains(maladeCourant.getNumero())) {
                    Object[] line = new Object[6];
                    line[0] = maladeCourant.getPrenom();
                    line[1] = maladeCourant.getNom();
                    line[2] = maladeCourant.getTel();
                    line[3] = maladeCourant.getMutuelle();
                    line[4] = hospitalisationCourante.getNo_malade();
                    line[5] = hospitalisationCourante.getLit();
                    myPatients.add(line);
                }
            }
        }
        results = new Object[myPatients.size()][6];
        for (int i = 0; i < myPatients.size(); i++) {
            results[i] = myPatients.get(i);
        }
        return results;
    }

    /**
     * Checks if the profile data changed, if they were changed, then saves all the modifications
     */
    public void modifyMyProfileData() {
        String hydratedNom = this.panelMonCompte.hydratedNom;
        String hydratedPrenom = this.panelMonCompte.hydratedPrenom;
        String hydratedTel = telephoneBeautify(this.panelMonCompte.hydratedTel.trim());

        String nomInTextField = this.panelMonCompte.getFieldNom().getText();
        String prenomInTextField = this.panelMonCompte.getFieldPrenom().getText();
        String telInTextField = telephoneBeautify(this.panelMonCompte.getFieldTelephone().getText().trim());

        int numeroEmploye = this.userLogin.getEmploye();

        if (!hydratedNom.equals(nomInTextField) || !hydratedPrenom.equals(prenomInTextField) || !hydratedTel.equals(telInTextField)) {
            if (connexion.getMethod().equals("MySQL-database")) {
                this.connexion.modification(
                        "Employe",
                        new String[] {"nom", "prenom", "tel"},
                        new String[] {nomInTextField, prenomInTextField, telInTextField},
                        new String[]{"numero"},
                        new String[]{Integer.toString(numeroEmploye)}
                );
            } else if (connexion.getMethod().equals("Data-filesystem")) {
                ArrayList<Object> collectionEmployes = this.fileMemory.getCollectionEmploye();
                int numberOfRows = collectionEmployes.size();
                for (int i = 0; i < numberOfRows; i++) {
                    Employe employeCourant = (Employe) collectionEmployes.get(i);
                    if (employeCourant.getNumero() == numeroEmploye) {
                        employeCourant.setNom(nomInTextField);
                        employeCourant.setPrenom(prenomInTextField);
                        employeCourant.setTel(telInTextField);
                        this.fileMemory.getCollectionEmploye().set(i, employeCourant);
                        this.fileMemory.saveAllObjects();
                    }
                }
            }
        }
    }

    /**
     * Retrieves a list with all the available services
     * @return - Returns a list
     */
    public ArrayList<HashMap<String, Object>> getAllAvailableServices() {
        ArrayList<HashMap<String, Object>> resultsServices = new ArrayList<>();
        if (connexion.getMethod().equals("MySQL-database")) {
            resultsServices = this.connexion.selection("Service", new String[] {}, new String[] {});
        } else if (connexion.getMethod().equals("Data-filesystem")) {
            ArrayList<Object> results = this.fileMemory.getCollectionService();
            for (Object o : results) {
                Service serviceCourant = (Service) o;
                HashMap<String, Object> line = new HashMap<>();
                line.put("code", serviceCourant.getCode());
                line.put("nom", serviceCourant.getNom());
                line.put("batiment", serviceCourant.getBatiment());
                line.put("directeur", serviceCourant.getDirecteur());
                resultsServices.add(line);
            }
        }
        return resultsServices;
    }

    /**
     * Retrieves a list with all the free bedrooms for the given service
     * @param code_service - Hospital code service
     * @return - Returns a dict
     */
    public HashMap<Integer, ArrayList<Integer>> getFreeChambresByCodeService(String code_service) {
        HashMap<Integer, ArrayList<Integer>> allChambres = new HashMap<>();
        HashMap<Integer, ArrayList<Integer>> chambresOccupes = new HashMap<>();
        HashMap<Integer, ArrayList<Integer>> chambresLibres = new HashMap<>();
        if (connexion.getMethod().equals("MySQL-database")) {
            ArrayList<HashMap<String, Object>> resultsHospitalisations = this.connexion.selection("Hospitalisation", new String[]{"code_service"}, new String[]{code_service});
            for (HashMap<String, Object> line : resultsHospitalisations) {
                if (chambresOccupes.containsKey((Integer) line.get("no_chambre"))) {
                    ArrayList<Integer> litsDejaTraites = chambresOccupes.get((Integer) line.get("no_chambre"));
                    litsDejaTraites.add((Integer) line.get("lit"));
                    chambresOccupes.put((Integer) line.get("no_chambre"), litsDejaTraites);
                } else {
                    ArrayList<Integer> litsOccupees = new ArrayList<>();
                    litsOccupees.add((Integer) line.get("lit"));
                    chambresOccupes.put((Integer) line.get("no_chambre"), litsOccupees);
                }
            }
            ArrayList<HashMap<String, Object>> resultsChambres = this.connexion.selection("Chambre", new String[]{"code_service"}, new String[]{code_service});
            for (HashMap<String, Object> line : resultsChambres) {
                Integer amount = (Integer) line.get("nb_lits");
                Integer no_chambre = (Integer) line.get("no_chambre");
                ArrayList<Integer> numberOfLitsInside = new ArrayList<>();
                for (int i = 1; i <= amount; i++) {
                    numberOfLitsInside.add(i);
                }
                allChambres.put(no_chambre, numberOfLitsInside);
            }
        } else if (connexion.getMethod().equals("Data-filesystem")) {
            ArrayList<Object> collectionHospitalisation = fileMemory.getCollectionHospitalisation();
            for (Object o : collectionHospitalisation) {
                Hospitalisation hospitalisationCourante = (Hospitalisation) o;
                if (hospitalisationCourante.getCode_service().equals(code_service)) {
                    if (chambresOccupes.containsKey(hospitalisationCourante.getNo_chambre())) {
                        ArrayList<Integer> litsDejaTraites = chambresOccupes.get(hospitalisationCourante.getNo_chambre());
                        litsDejaTraites.add(hospitalisationCourante.getLit());
                        chambresOccupes.put(hospitalisationCourante.getNo_chambre(), litsDejaTraites);
                    } else {
                        ArrayList<Integer> litsOccupees = new ArrayList<>();
                        litsOccupees.add(hospitalisationCourante.getLit());
                        chambresOccupes.put(hospitalisationCourante.getNo_chambre(), litsOccupees);
                    }
                }
            }
            ArrayList<Object> collectionChambres = fileMemory.getCollectionChambres();
            for (Object o : collectionChambres) {
                Chambre chambreCourante = (Chambre) o;
                if (chambreCourante.getCode_service().equals(code_service)) {
                    ArrayList<Integer> numberOfLitsInside = new ArrayList<>();
                    for (int i = 1; i <= chambreCourante.getNb_lits(); i++) {
                        numberOfLitsInside.add(i);
                    }
                    allChambres.put(chambreCourante.getNo_chambre(), numberOfLitsInside);
                }
            }
        }
        Integer qtyChambresLibres = allChambres.size();
        for (Integer chambre: allChambres.keySet()) {
            if (chambresOccupes.containsKey(chambre)) {
                ArrayList<Integer> allLits = allChambres.get(chambre);
                ArrayList<Integer> litsOccupes = chambresOccupes.get(chambre);
                ArrayList<Integer> litsLibres = new ArrayList<>();
                for (Integer lit : allLits) {
                    if (!litsOccupes.contains(lit)) litsLibres.add(lit);
                }
                if (!litsLibres.isEmpty()) {
                    chambresLibres.put(chambre, litsLibres);
                }
            } else {
                chambresLibres.put(chambre, allChambres.get(chambre));
            }
        }
        return chambresLibres;
    }

    /**
     * Checks if the patient was registered on our hospital already
     * @param nom - Surname of the patient
     * @param prenom - First name of the patient
     * @param tel - Telephone number of the patient
     * @return - Returns the patients number if already registered, or 0 otherwise
     */
    public Integer checkIfPatientExistsInHospitalData(String nom, String prenom, String tel) {
        String telBeautified = telephoneBeautify(tel);
        if (connexion.getMethod().equals("MySQL-database")) {
            ArrayList<HashMap<String, Object>> resultsMalade = this.connexion.selection("Malade", new String[]{"nom", "prenom", "tel"}, new String[]{nom, prenom, telBeautified});
            if (resultsMalade.size() > 0) {
                return Integer.parseInt(resultsMalade.get(0).get("numero").toString());
            }
        } else if (connexion.getMethod().equals("Data-filesystem")) {
            ArrayList<Object> collectionMalades = this.fileMemory.getCollectionMalade();
            for (Object o : collectionMalades) {
                Malade maladeCourant = (Malade) o;
                if (maladeCourant.getNom().equals(nom) && maladeCourant.getPrenom().equals(prenom) && maladeCourant.getTel().equals(telBeautified))
                    return maladeCourant.getNumero();
            }
        }
        return 0;
    }

    /**
     * Retrieves a list with all the hospital doctors
     * @return - Returns a list with all the doctors
     */
    public ArrayList<HashMap<String, Object>> retieveAllDoctors() {
        ArrayList<HashMap<String, Object>> doctorInfo = new ArrayList<>();
        if (connexion.getMethod().equals("MySQL-database")) {
            ArrayList<HashMap<String, Object>> resultsDocteurs = this.connexion.selection("Docteur", new String[] {}, new String[]{});
            for (HashMap<String, Object> docteur : resultsDocteurs) {
                ArrayList<HashMap<String, Object>> employeCourant = this.connexion.selection("Employe", new String[] {"numero"}, new String[]{docteur.get("numero").toString()});
                HashMap<String, Object> docteurJointure = new HashMap<>();
                docteurJointure.put("numero", docteur.get("numero"));
                if (docteur.get("specialite") == null) docteurJointure.put("specialite", "Aucune");
                else docteurJointure.put("specialite", docteur.get("specialite"));
                docteurJointure.put("nom", employeCourant.get(0).get("nom"));
                docteurJointure.put("prenom", employeCourant.get(0).get("prenom"));
                doctorInfo.add(docteurJointure);
            }
        } else if (connexion.getMethod().equals("Data-filesystem")) {
            ArrayList<Object> collectionDocteurs = this.fileMemory.getCollectionDocteurs();
            ArrayList<Object> collectionEmployes = this.fileMemory.getCollectionEmploye();
            for (Object o : collectionDocteurs) {
                HashMap<String, Object> line = new HashMap<>();
                Docteur docteur = (Docteur) o;
                line.put("numero", docteur.getNumero());
                if (docteur.getSpecialite() == null) line.put("specialite", "Aucune");
                else line.put("specialite", docteur.getSpecialite());
                for (Object o2 : collectionEmployes) {
                    Employe employe = (Employe) o2;
                    if (docteur.getNumero() == employe.getNumero()) {
                        line.put("nom", employe.getNom());
                        line.put("prenom", employe.getPrenom());
                        break;
                    }
                }
                doctorInfo.add(line);
            }
        }
        return doctorInfo;
    }

    /**
     * Search for the patient's current doctors that already treated him at least once before
     * @param no_malade - Patient's number
     * @return - Returns a list with all the doctors that treated the patient once before
     */
    public ArrayList<HashMap<String, Object>> retrievePatientDoctor(int no_malade) {
        ArrayList<HashMap<String, Object>> doctorInfo = new ArrayList<>();
        if (connexion.getMethod().equals("MySQL-database")) {
            ArrayList<HashMap<String, Object>> myDoctorsNo = this.connexion.selection("Soigne", new String[]{"no_malade"}, new String[]{Integer.toString(no_malade)});
            for (HashMap<String, Object> line : myDoctorsNo) {
                ArrayList<HashMap<String, Object>> resultsDocteur = this.connexion.selection("Docteur", new String[]{"numero"}, new String[]{line.get("no_docteur").toString()});
                ArrayList<HashMap<String, Object>> resultsEmploye = this.connexion.selection("Employe", new String[]{"numero"}, new String[]{line.get("no_docteur").toString()});
                HashMap<String, Object> docteurJointure = new HashMap<>();
                docteurJointure.put("numero", line.get("no_docteur").toString());
                if (resultsDocteur.get(0).get("specialite") == null) docteurJointure.put("specialite", "Aucune");
                else docteurJointure.put("specialite", resultsDocteur.get(0).get("specialite").toString());
                docteurJointure.put("nom", resultsEmploye.get(0).get("nom").toString());
                docteurJointure.put("prenom", resultsEmploye.get(0).get("prenom").toString());
                doctorInfo.add(docteurJointure);
            }
        } else if (connexion.getMethod().equals("Data-filesystem")) {
            ArrayList<Integer> myDoctorsNo = new ArrayList<>();
            ArrayList<Object> collectionSoigne = this.fileMemory.getCollectionSoigne();
            for (Object o : collectionSoigne) {
                Soigne soigne = (Soigne) o;
                if (soigne.getNo_malade() == no_malade) myDoctorsNo.add(soigne.getNo_docteur());
            }
            ArrayList<Object> collectionDocteurs = this.fileMemory.getCollectionDocteurs();
            ArrayList<Object> collectionEmployes = this.fileMemory.getCollectionEmploye();
            for (Object o : collectionDocteurs) {
                HashMap<String, Object> line = new HashMap<>();
                Docteur docteur = (Docteur) o;
                if (myDoctorsNo.contains(docteur.getNumero())) {
                    line.put("numero", docteur.getNumero());
                    if (docteur.getSpecialite() == null) line.put("specialite", "Aucune");
                    else line.put("specialite", docteur.getSpecialite());
                    for (Object o2 : collectionEmployes) {
                        Employe employe = (Employe) o2;
                        if (docteur.getNumero() == employe.getNumero()) {
                            line.put("nom", employe.getNom());
                            line.put("prenom", employe.getPrenom());
                            break;
                        }
                    }
                    doctorInfo.add(line);
                }
            }
        }
        return doctorInfo;
    }

    /**
     * Retrieves the patient's Health Care insurance and Physical address by the Patients number
     * @param no_malade - Patients number on our files
     * @return - Returns a dict with Health Care insurance and Address
     */
    public HashMap<String, String> getPatientsMutuelleAndAdresse(int no_malade) {
        HashMap<String, String> patientMutuelleAndAdresse = new HashMap<>();
        if (connexion.getMethod().equals("MySQL-database")) {
            ArrayList<HashMap<String, Object>> maladeInfo = this.connexion.selection("Malade", new String[] {"numero"}, new String[] {Integer.toString(no_malade)});
            patientMutuelleAndAdresse.put("mutuelle", maladeInfo.get(0).get("mutuelle").toString());
            patientMutuelleAndAdresse.put("adresse", maladeInfo.get(0).get("adresse").toString());
        } else if (connexion.getMethod().equals("Data-filesystem")) {
            ArrayList<Object> collectionMalades = this.fileMemory.getCollectionMalade();
            for (Object o : collectionMalades) {
                Malade malade = (Malade) o;
                if (malade.getNumero() == no_malade) {
                    patientMutuelleAndAdresse.put("mutuelle", malade.getMutuelle());
                    patientMutuelleAndAdresse.put("adresse", malade.getAdresse());
                    break;
                }
            }
        }
        return patientMutuelleAndAdresse;
    }

    /**
     * Registers a patient once all the data from the form was retrieved
     * @param no_malade - Unique number of the patient or 0 if never registered before
     * @param nomMalade - Surname of the patient
     * @param prenomMalade - First name of the patient
     * @param telMalade - Telephone number of the patient
     * @param mutuelleMalade - Health Care insurance of the patient
     * @param adresseMalade - Physical address of the patient
     * @param no_Docteur - Unique doctor's number chosen to assist the patient
     * @param code_service - Service in which the patient will be registered
     * @param no_chambre - Number of the bedroom for the hospitalisation
     * @param lit - Number of the bed for the hospitalisation
     */
    public void registerNewPatient(int no_malade, String nomMalade, String prenomMalade, String telMalade, String mutuelleMalade, String adresseMalade, int no_Docteur, String code_service, int no_chambre, int lit) {
        if (connexion.getMethod().equals("MySQL-database")) {
            this.connexion.registerNewPatient(no_malade, nomMalade, prenomMalade, telephoneBeautify(telMalade), mutuelleMalade, adresseMalade, no_Docteur, code_service, no_chambre, lit);
        } else if (connexion.getMethod().equals("Data-filesystem")) {
            this.fileMemory.registerNewPatient(no_malade, nomMalade, prenomMalade, telephoneBeautify(telMalade), mutuelleMalade, adresseMalade, no_Docteur, code_service, no_chambre, lit);
        }
    }

    /**
     * Frees the patient due to its good condition, by using the surname, first name and telephone number of the patient
     * @param nom - Surname of the patient to release
     * @param prenom - First name of the patient to release
     * @param tel - Telephone number of patient to release
     */
    public void libererPatient(String nom, String prenom, String tel) {
        if (connexion.getMethod().equals("MySQL-database")) {
            this.connexion.libererPatient(nom, prenom, telephoneBeautify(tel));
        } else if (connexion.getMethod().equals("Data-filesystem")) {
            this.fileMemory.libererPatient(nom, prenom, telephoneBeautify(tel));
        }
    }

    /**
     * Retrieves the amount of bedrooms
     * @return - Returns the amount of bedrooms
     */
    public int getTotalAmountOfBedrooms() {
        if (connexion.getMethod().equals("MySQL-database")) {
            return this.connexion.getTotalAmountOfBedrooms();
        } else if (connexion.getMethod().equals("Data-filesystem")) {
            return this.fileMemory.getTotalAmountOfBedrooms();
        }
        return 0;
    }

     /**
     * Retrieves the amount of occupied bedrooms
     * @return - Returns the amount of bedrooms occupied
     */
    public int getAmountOfOccupiedBedrooms() {
        if (connexion.getMethod().equals("MySQL-database")) {
            return this.connexion.getAmountOfOccupiedBedrooms();
        } else if (connexion.getMethod().equals("Data-filesystem")) {
            return this.fileMemory.getAmountOfOccupiedBedrooms();
        }
        return 0;
    }

    /**
     * Retrieves the amount of staff personnel working during the day
     * @return - Returns a precise amount
     */
    public int getAmountOfEmployesWorkingByDay() {
        if (connexion.getMethod().equals("MySQL-database")) {
            return this.connexion.getAmountOfEmployesWorkingByDay();
        } else if (connexion.getMethod().equals("Data-filesystem")) {
            return this.fileMemory.getAmountOfEmployesWorkingByDay();
        }
        return 0;
    }

    /**
     * Retrieves the amount of staff personnel working during the night
     * @return - Returns a precise amount
     */
    public int getAmountOfEmployesWorkingByNight() {
        if (connexion.getMethod().equals("MySQL-database")) {
            return this.connexion.getAmountOfEmployesWorkingByNight();
        } else if (connexion.getMethod().equals("Data-filesystem")) {
            return this.fileMemory.getAmountOfEmployesWorkingByNight();
        }
        return 0;
    }

    /**
     * Retrieves the amount of nurses for each service
     * @return - Returns a list indexed by the service
     */
    public HashMap<String, Integer> getAmountOfInfirmiersByService() {
        if (connexion.getMethod().equals("MySQL-database")) {
            return this.connexion.getAmountOfInfirmiersByService();
        } else if (connexion.getMethod().equals("Data-filesystem")) {
            return this.fileMemory.getAmountOfInfirmiersByService();
        }
        return null;
    }

    /**
     * Retrieves the amount of hospitalised patients for each service
     * @return - Returns a list indexed by the service
     */
    public HashMap<String, Integer> getAmountOfPatientsByService() {
        if (connexion.getMethod().equals("MySQL-database")) {
            return this.connexion.getAmountOfPatientsByService();
        } else if (connexion.getMethod().equals("Data-filesystem")) {
            return this.fileMemory.getAmountOfPatientsByService();
        }
        return null;
    }

    /**
     * Retrieves all the non verified logins
     * @return - Returns a list of unactivated logins
     */
    public ArrayList<String> getUnactivatedLoginsList() {
        ArrayList<String> allPendingLogins = new ArrayList<>();
        if (connexion.getMethod().equals("MySQL-database")) {
            ArrayList<HashMap<String, Object>> nonActivatedLogins = this.connexion.selection("Logins", new String[]{"validated"}, new String[]{"0"});
            for (HashMap<String, Object> login : nonActivatedLogins) {
                allPendingLogins.add(login.get("idLogin").toString()+"-"+login.get("username").toString());
            }
        } else if (connexion.getMethod().equals("Data-filesystem")) {
            for (Object o : this.fileMemory.getCollectionLogin()) {
                Login login = (Login) o;
                if (!login.isValidated())
                    allPendingLogins.add(login.getIdLogin()+"-"+login.getUsername());
            }
        }
        return allPendingLogins;
    }

    /**
     * Activates a login by its id
     * @param idLogin - Id of the login
     */
    public void activateLogin(int idLogin) {
        if (connexion.getMethod().equals("MySQL-database")) {
            this.connexion.modification("Login", new String[]{"validated"}, new String[]{"1"}, new String[]{"idLogin"}, new String[]{Integer.toString(idLogin)});
        } else if (connexion.getMethod().equals("Data-filesystem")) {
            ArrayList<Object> collectionLogins = this.fileMemory.getCollectionLogin();
            for (int i = 0; i < collectionLogins.size(); i++) {
                Login login = (Login) collectionLogins.get(i);
                if (login.getIdLogin() == idLogin) {
                    login.setValidated(true);
                    this.fileMemory.getCollectionLogin().set(i, login);
                }
            }
            this.fileMemory.saveAllObjects();
        }
    }

    // ---------------------------------------- Main

    /**
     * Main method (starting point)
     * @param args - Arguments that are given to the application
     */
    public static void main(String[] args) {
        boolean restart = false;
        if (args.length > 0) restart = args[0].equals("restart");
        Controleur controleur = new Controleur(restart);
    }

    // ---------------------------------------- EventHandlers

    @Override
    public void actionPerformed(ActionEvent e) {
        switch (e.getActionCommand()) {
            case "Se connecter": {
                if (this.checkCredentials()) this.initialisePanelDeGarde();
                break;
            }
            case "S'inscrire": {
                this.initialisePanelInscription();
                break;
            }
            case "Retour vers login": {
                this.initialisePanelLogin();
                break;
            }
            case "Valider inscription": {
                if (this.valdidateInscription()) {
                    if (connexion.getMethod().equals("MySQL-database")) {
                        if (connexion.inscription(
                                this.panelInscription.getFieldNom().getText(),
                                this.panelInscription.getFieldPrenom().getText(),
                                telephoneBeautify(this.panelInscription.getFieldTelephone().getText()),
                                (String) this.panelInscription.getListFonction().getSelectedItem(),
                                this.panelInscription.getFieldIdentifiant().getText(),
                                String.valueOf(this.panelInscription.getFieldMotDePasse().getPassword())
                        )) {
                            this.initialisePanelLogin();
                        }
                    } else if (connexion.getMethod().equals("Data-filesystem")) {
                        if (fileMemory.inscription(
                                this.panelInscription.getFieldNom().getText(),
                                this.panelInscription.getFieldPrenom().getText(),
                                telephoneBeautify(this.panelInscription.getFieldTelephone().getText()),
                                (String) this.panelInscription.getListFonction().getSelectedItem(),
                                this.panelInscription.getFieldIdentifiant().getText(),
                                String.valueOf(this.panelInscription.getFieldMotDePasse().getPassword())
                        )) {
                            this.initialisePanelLogin();
                        }
                    }
                } else {
                    panelInscription.showErrorMessage();
                }
                break;
            }
            case "Acceder mon compte": {
                this.initialisePopupMonCompte();
                break;
            }
            case "Modifier mon compte": {
                this.modifyMyProfileData();
                this.panelMonCompte.dispatchEvent(new WindowEvent(this.panelMonCompte, WindowEvent.WINDOW_CLOSING));
                this.initialisePanelDeGarde();
                break;
            }
            case "Ouvrir popup nouveau malade": {
                this.initialisePopupAjouterMalade();
                break;
            }
            case "Enregistrer nouveau patient": {
                Integer no_docteur;
                if (this.panelAjouterMalade.usingNewDoctor) {
                    no_docteur = (Integer) this.panelAjouterMalade.listeOfAllDoctors.get(this.panelAjouterMalade.comboDocteur.getSelectedIndex()).get("numero");
                } else {
                    no_docteur = (Integer) this.panelAjouterMalade.listeOfMyDoctors.get(this.panelAjouterMalade.comboDocteur.getSelectedIndex()).get("numero");
                }
                this.registerNewPatient(
                        this.panelAjouterMalade.noMalade,
                        this.panelAjouterMalade.selectedNom,
                        this.panelAjouterMalade.selectedPrenom,
                        this.panelAjouterMalade.selectedTelephone,
                        this.panelAjouterMalade.fieldMutuelle.getText(),
                        this.panelAjouterMalade.fieldAdresse.getText(),
                        no_docteur,
                        this.panelAjouterMalade.selectedCodeService,
                        this.panelAjouterMalade.selectedNoChambre,
                        this.panelAjouterMalade.selectedNoLit
                );
                this.panelAjouterMalade.dispatchEvent(new WindowEvent(this.panelAjouterMalade, WindowEvent.WINDOW_CLOSING));
                this.initialisePanelDeGarde();
            }
            case "Ouvrir popup liberer malade": {
                this.initialisePopupLibererMalade();
                break;
            }
            case "Liberer malade": {
                String nomMalade = this.panelLiberer.getFieldNom().getText();
                String prenomMalade = this.panelLiberer.getFieldPrenom().getText();
                String telMalade = this.panelLiberer.getFiledTelephone().getText();
                this.libererPatient(nomMalade, prenomMalade, telMalade);
                this.panelLiberer.dispatchEvent(new WindowEvent(this.panelLiberer, WindowEvent.WINDOW_CLOSING));
                this.initialisePanelDeGarde();
                break;
            }
            case "Acceder reporting": {
                this.initialisePanelReporting();
                break;
            }
            case "Retourner page de garde": {
                this.initialisePanelDeGarde();
                break;
            }
            case "Activer un compte": {
                String selectedLoginToActivate = this.panelMonCompte.selectedLoginToActivate;
                String[] splited = selectedLoginToActivate.split("-");
                int idLogin = Integer.parseInt(splited[0]);
                String username = splited[1];
                this.activateLogin(idLogin);
                this.panelMonCompte.dispatchEvent(new WindowEvent(this.panelMonCompte, WindowEvent.WINDOW_CLOSING));
                this.initialisePanelDeGarde();
                break;
            }
            default: break;
        }
    }
}
