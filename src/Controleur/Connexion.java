package Controleur;

import java.sql.*;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * Class Connexion for the MySQL functionality
 */
public class Connexion implements TraitementDonnees{

    private String method = "";
    private Connection connect = null;
    private Statement statement = null;
    private PreparedStatement preparedStatement = null;
    private ResultSet resultSet = null;

    /**
     * Constructor of the class
     */
    public Connexion() {
        try {
            Class c = Class.forName("com.mysql.cj.jdbc.Driver");
            Driver pilote = (Driver) c.newInstance();
            DriverManager.registerDriver(pilote);

            String protocole = "jdbc:mysql:";
            String ip = "remotemysql.com";
            String port = "3306";
            String nomBase = "kAhTna6s5C";

            String url = protocole + "//" + ip + ":" + port + "/" + nomBase;
            String nomConnexion = "kAhTna6s5C";
//            String motDePasse = "iYS0DK59A6";
            String motDePasse = "wrong"; // TODO: Delete later

            this.connect = DriverManager.getConnection(url, nomConnexion, motDePasse);
            System.out.println("Connection to Database has been established.");
        } catch (Exception e) {
            System.out.println(e.getMessage());
        } finally {
            if (this.connect != null) this.method = "MySQL-database";
            else this.method = "Data-filesystem";
            System.out.println("Using " + this.method + " method!");
        }
    }

    // ---------------------------------------- Getters/Setters

    /**
     * Getter for the method
     * @return method - MySQL-database or Data-filesystem
     */
    public String getMethod() {
        return this.method;
    }

    /**
     * Retrieves the amount of bedrooms
     * @return - Returns the amount of bedrooms
     */
    public int getTotalAmountOfBedrooms() {
        int amount = 0;
        try {
            this.resultSet = this.executePreparedSelect("SELECT SUM(nb_lits) FROM Chambre", new String[]{});
            amount = this.resultSet.getInt(0);
            if (this.resultSet != null) {
                while (this.resultSet.next()) {
                    ResultSetMetaData rsmd = resultSet.getMetaData();
                    for (int i = 1; i <= rsmd.getColumnCount(); i++) {
                        amount = (int) this.resultSet.getObject(i);
                    }
                }
            }
        } catch (SQLException e) {
            System.err.println(e.getMessage());
        }
        return amount;
    }

    /**
     * Retrieves the amount of occupied bedrooms
     * @return - Returns the amount of bedrooms occupied
     */
    public int getAmountOfOccupiedBedrooms() {
        ArrayList<HashMap<String, Object>> resultsOfHospitalisations = this.selection("Hospitalisation", new String[]{}, new String[]{});
        return resultsOfHospitalisations.size();
    }

    /**
     * Retrieves the amount of staff personnel working during the day
     * @return - Returns a precise amount
     */
    public int getAmountOfEmployesWorkingByDay() {
       return this.selection("Infirmier", new String[]{"rotation"}, new String[]{"JOUR"}).size();
    }

    /**
     * Retrieves the amount of staff personnel working during the night
     * @return - Returns a precise amount
     */
    public int getAmountOfEmployesWorkingByNight() {
        return this.selection("Infirmier", new String[]{"rotation"}, new String[]{"NUIT"}).size();
    }

    /**
     * Retrieves the amount of nurses for each service
     * @return - Returns a list indexed by the service
     */
    public HashMap<String, Integer> getAmountOfInfirmiersByService() {
        HashMap<String, Integer> savedServices = new HashMap<>();
        ArrayList<HashMap<String, Object>> resultsInfirmiers = this.selection("Infirmier", new String[] {}, new String[]{});
        for (HashMap<String, Object> line : resultsInfirmiers) {
            if (line.get("code_service") != null) {
                String currentService = line.get("code_service").toString();
                if (currentService == null) continue;
                if (savedServices.containsKey(currentService)) {
                    savedServices.put(currentService, savedServices.get(currentService)+1);
                } else {
                    savedServices.put(currentService, 1);
                }
            }
        }
        return savedServices;
    }

    /**
     * Retrieves the amount of hospitalised patients for each service
     * @return - Returns a list indexed by the service
     */
    public HashMap<String, Integer> getAmountOfPatientsByService() {
        HashMap<String, Integer> savedServices = new HashMap<>();
        ArrayList<HashMap<String, Object>> resultsHospitalisations = this.selection("Hospitalisation", new String[] {}, new String[]{});
        for (HashMap<String, Object> line : resultsHospitalisations) {
            String currentService = line.get("code_service").toString();
            if (savedServices.containsKey(currentService)) {
                savedServices.put(currentService, savedServices.get(currentService)+1);
            } else {
                savedServices.put(currentService, 1);
            }
        }
        return savedServices;
    }

    // ---------------------------------------- Essential methods

    /**
     * Generic execution of prepared statement by using execute()
     * @param query - Query to execute
     * @param values - Values to replace the '?'
     * @return - True or False
     * @throws SQLException - Throws SQLException if the Query is not good or if there is a problem with the database
     */
    private boolean executePreparedStatement(String query, String[] values) throws SQLException {
        preparedStatement = connect.prepareStatement(query);
        int cpt = values.length;
        for (int i = 0; i < cpt ; i++) {
            preparedStatement.setObject(i + 1, values[i]);
        }
        return preparedStatement.execute();
    }

    /**
     * Executes a prepared statement with executeQuery()
     * @param query - Query to execute
     * @param values - Values to replace the '?'
     * @return ResultSet
     * @throws SQLException - Throws SQLException if the Query is not good or if there is a problem with the database
     */
    private ResultSet executePreparedSelect(String query, String[] values) throws SQLException {
        preparedStatement = connect.prepareStatement(query);
        int cpt = values.length;
        for (int i = 0; i < cpt ; i++) {
            preparedStatement.setObject(i + 1, values[i]);
        }
        this.resultSet = preparedStatement.executeQuery();
        return this.resultSet;
    }

    /**
     * Executes a prepared statement with executeUpdate()
     * @param query - Query to execute
     * @param values - Values to replace the '?'
     * @return - True or False
     * @throws SQLException - Throws SQLException if the Query is not good or if there is a problem with the database
     */
    private boolean executePreparedUpdate(String query, String[] values) throws SQLException {
        preparedStatement = connect.prepareStatement(query);
        int cpt = values.length;
        for (int i = 0; i < cpt ; i++) {
            preparedStatement.setObject(i + 1, values[i]);
        }
        return (preparedStatement.executeUpdate() > 0);
    }

    /**
     * Transforms an array with all the names into a prepared string with the respective conditions
     * Example : { "username", "password" } = " username = ? AND password = ? "
     * @param columnNames - Names of columns in question
     * @return - Returns a prepared condition for WHERE or SET
     */
    private String columnEqualsSomethingInSQL(String[] columnNames) {
        StringBuilder treatedSQL = new StringBuilder(" ");
        int columnsQty = columnNames.length;
        for (int i = 0; i < columnsQty; i++) {
            treatedSQL.append(columnNames[i]).append(" = ").append("?");
            if (i < columnsQty - 1) treatedSQL.append(" AND ");
        }
        return treatedSQL.toString();
    }

    // ---------------------------------------- Override Methods

    /**
     * Creates a new user with the given data
     * @param nom - Surname of the user
     * @param prenom - First name of the user
     * @param tel - Telephone number of the user
     * @param fonction - Function/Job of the user
     * @param username - Username for the new user
     * @param password - Password for the new user
     * @return - Returns true if user was successfully created, and false otherwise
     */
    @Override
    public boolean inscription(String nom, String prenom, String tel, String fonction, String username, String password) {
        // we insert the new Employe if not yet registered
        this.insertion("Employe", new String[] {"nom", "prenom", "tel"}, new String[] {nom, prenom, tel});

        // We retrieve the number of the Employe in order to connect it with the username on Login table
        ArrayList<HashMap<String, Object>> results = this.selection("Employe", new String[] {"nom", "prenom", "tel"}, new String[] {nom, prenom, tel});
        int numeroEmploye = 0;
        if (results.size() == 1) {
            numeroEmploye = (int) results.get(0).get("numero");
        }

        if (fonction.equals("Infirmier")) {
            this.insertion(fonction, new String[] {"numero", "rotation"}, new String[] {Integer.toString(numeroEmploye), "JOUR"});
        } else if (fonction.equals("Docteur")) {
            this.insertion(fonction, new String[] {"numero"}, new String[] {Integer.toString(numeroEmploye)});
        }

        // We finally insert into Login the new username and password, with the employe number if possible
        return this.insertion("Login", new String[] {"username", "password", "employe", "fonction"}, new String[] {username, password, Integer.toString(numeroEmploye), fonction});
    }

    /**
     * Verifies if the username and password exist and correspond to each other
     * @param  username - User's username
     * @param password - User's password
     * @return Returns true if the username and password are correct, and false otherwise
     */
    @Override
    public boolean authentification(String username, String password) {
        try {
            String query = "SELECT * FROM Login WHERE username = ? AND password = ?";
            if (this.executePreparedStatement(query, new String[] { username, password })) { // query valid
                this.resultSet = this.preparedStatement.getResultSet();
                if (this.resultSet == null) {
                    return false;
                } else {
                    while (this.resultSet.next()) {
                        if (this.resultSet.getString("username").equals(username)
                                && this.resultSet.getString("password").equals(password)) {
                            return true;
                        }
                    }
                }
            } else {
                return false;
            }
        } catch (SQLException e) {
            System.err.println("Error checking authentication (MYSQL-Connexion.java)" + e.getMessage());
        }
        return false;
    }

    /**
     * Generic SELECT function for the database
     * @param tableName - Corresponds to the table name
     * @param columnNames - Corresponds to the columns for the WHERE conditions
     * @param values - Corresponds to the values for the WHERE conditions
     * @return Returns the results of the selection
     */
    @Override
    public ArrayList<HashMap<String, Object>> selection(String tableName, String[] columnNames, String[] values) {
        ArrayList<HashMap<String, Object>> results = new ArrayList<>();
        try {
            StringBuilder query = new StringBuilder("SELECT * FROM " + tableName);
            if (columnNames.length == values.length && columnNames.length > 0) {
                query.append(" WHERE ");
                query.append(this.columnEqualsSomethingInSQL(columnNames));
            }
            if (this.executePreparedStatement(query.toString(), values)) { // query valid
                this.resultSet = this.preparedStatement.getResultSet();
                if (this.resultSet != null) {
                    while (this.resultSet.next()) {
                        ResultSetMetaData rsmd = resultSet.getMetaData();
                        HashMap<String, Object> line = new HashMap<>();
                        for (int i = 1; i <= rsmd.getColumnCount(); i++) {
                            String columnName = rsmd.getColumnName(i);
                            line.put(columnName, this.resultSet.getObject(i));
                        }
                        results.add(line);
                    }
                }
            }
        } catch (SQLException e) {
            System.err.println(e.getMessage());
        }
        return results;
    }

    /**
     * Generic INSERT function for the database
     * @param tableName - Corresponds to the table name
     * @param columnNames - Corresponds to the columns of the insertion
     * @param newValues - Corresponds to the new values for the insertion
     * @return Returns true if the insertions were successful, and false otherwise
     */
    @Override
    public boolean insertion(String tableName, String[] columnNames, String[] newValues) {
        try {
            int columnNamesQty = columnNames.length,
                newValuesQty = newValues.length;
            if ((columnNamesQty == newValuesQty) && columnNamesQty > 0) {
                StringBuilder query = new StringBuilder("INSERT INTO " + tableName);
                query.append(" (").append(String.join(",", columnNames)).append(") ");
                query.append(" VALUES (?");
                for (int i = 1; i < columnNamesQty; i++) query.append(", ?");
                query.append(") ");
                return this.executePreparedUpdate(query.toString(), newValues);
            }
        } catch (SQLException e) {
            System.err.println(e.getMessage());
        }
        return false;
    }

    /**
     * Generic UPDATE function for the database
     * @param tableName - Corresponds to the table name
     * @param columnNames - Corresponds to the columns that need to be modified
     * @param newValues - Corresponds to the new values that replace the old ones
     * @param conditionColumns - Corresponds to the columns for the WHERE conditions
     * @param conditionValues - Corresponds to the values for the WHERE conditions
     * @return Returns true if the modifications were successful, and false otherwise
     */
    @Override
    public boolean modification(String tableName, String[] columnNames, String[] newValues, String[] conditionColumns, String[] conditionValues) {
        try {
            StringBuilder query = new StringBuilder("UPDATE " + tableName);
            if (columnNames.length == newValues.length) {
                query.append(" SET ");
                query.append(this.columnEqualsSomethingInSQL(columnNames));
            }
            if (conditionColumns.length == conditionValues.length) {
                query.append(" WHERE ");
                query.append(this.columnEqualsSomethingInSQL(conditionColumns));
            }
            String[] valuesToReplace = new String[newValues.length + conditionValues.length];
            int pos = 0;
            for (String value : newValues) {
                valuesToReplace[pos] = value;
                pos++;
            }
            for (String value : conditionValues) {
                valuesToReplace[pos] = value;
                pos++;
            }
//            if (this.executePreparedStatement(query.toString(), valuesToReplace)) { // query valid
//                this.resultSet = this.preparedStatement.getResultSet();
//                return this.resultSet != null;
//            }
            return this.executePreparedUpdate(query.toString(), valuesToReplace);
        } catch (SQLException e) {
            System.err.println(e.getMessage());
        }
        return false;
    }

    /**
     * Deletes a tuple or object from the database/file memory
     * @param tableName - Corresponds to the table name
     * @param conditionColumns - Corresponds to the columns for the WHERE conditions
     * @param conditionValues - Corresponds to the values for the WHERE conditions
     * @return Returns true if the deletes were successful, and false otherwise
     */
    @Override
    public boolean suppression(String tableName, String[] conditionColumns, String[] conditionValues) {
        try {
            int conditionColumnsQty = conditionColumns.length,
                conditionValuesQty = conditionValues.length;
            if ((conditionColumnsQty == conditionValuesQty) && conditionColumnsQty > 0) {
                StringBuilder query = new StringBuilder("DELETE FROM " + tableName);
                query.append(" WHERE ");
                query.append(this.columnEqualsSomethingInSQL(conditionColumns));
//                if (this.executePreparedStatement(query.toString(), conditionValues)) { // query valid
//                    this.resultSet = this.preparedStatement.getResultSet();
//                    return this.resultSet != null;
//                }
                return this.executePreparedUpdate(query.toString(), conditionValues);
            }
        } catch (SQLException e) {
            System.err.println(e.getMessage());
        }
        return false;
    }

    // ---------------------------------------- Core methods

    /**
     * Registers a new hospitalisation and adds the patient to the database/file system if it is not in it already
     * @param no_malade - Corresponds to the unique number for the patient
     * @param nomMalade - Surname of the patient
     * @param prenomMalade - First name of the patient
     * @param telMalade - Telephone number of the patient
     * @param mutuelleMalade - Health Care insurance
     * @param adresseMalade - Patient's current address
     * @param no_Docteur - The number of doctor assigned to the patient
     * @param code_service - The service in which the patient is being treated
     * @param no_chambre - The assigned room for the patient
     * @param lit - The number of the bed for the patient
     */
    public void registerNewPatient(int no_malade, String nomMalade, String prenomMalade, String telMalade, String mutuelleMalade, String adresseMalade, int no_Docteur, String code_service, int no_chambre, int lit) {
        if (no_malade == 0) {
            // insertion malade
            this.insertion("Malade", new String[]{"nom", "prenom", "tel", "mutuelle", "adresse"}, new String[] {nomMalade, prenomMalade, telMalade, mutuelleMalade, adresseMalade});
            ArrayList<HashMap<String, Object>> resultsMalade = this.selection("Malade", new String[]{"nom", "prenom", "tel", "mutuelle", "adresse"}, new String[] {nomMalade, prenomMalade, telMalade});
            no_malade = (int) resultsMalade.get(0).get("numero");
        } else {
            // update malade
            this.modification("Malade", new String[]{"mutuelle", "adresse"}, new String[] {mutuelleMalade, adresseMalade}, new String[]{"numero"}, new String[] {Integer.toString(no_malade)});
        }

        // Soigne
        ArrayList<HashMap<String, Object>> resultsSoigne = this.selection("Soigne", new String[]{"no_docteur", "no_malade"}, new String[]{Integer.toString(no_Docteur), Integer.toString(no_malade)});
        if (resultsSoigne.size() == 0) {
            this.insertion("Soigne", new String[]{"no_docteur", "no_malade"}, new String[]{Integer.toString(no_Docteur), Integer.toString(no_malade)});
        }

        // Hospitalisation
        this.insertion("Hospitalisation", new String[] {"no_malade", "code_service", "no_chambre", "lit"}, new String[] {
                Integer.toString(no_malade), code_service, Integer.toString(no_chambre), Integer.toString(lit)
        });
    }

    /**
     * Removes patient from being hospitalised (doesn't delete the Dossier of the patient)
     * @param nom - Surname of the patient to be set free
     * @param prenom - First name of the patient to be set free
     * @param tel - Telephone number of the patient to be set free
     */
    public void libererPatient(String nom, String prenom, String tel) {
        ArrayList<HashMap<String, Object>> resultsMalade = this.selection("Malade", new String[] {nom, prenom, tel}, new String[] {nom, prenom, tel});
        if (resultsMalade.size() > 0) {
            Integer no_malade = (Integer) resultsMalade.get(0).get("numero");
            this.suppression("Hospitalisation", new String[]{"no_malade"}, new String[]{Integer.toString(no_malade)});
            this.suppression("Soigne", new String[]{"no_malade"}, new String[]{Integer.toString(no_malade)});
        }
    }
}
