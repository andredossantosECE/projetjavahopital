package Modele;

import java.io.Serializable;

/**
 * Class Employe (Employee)
 */
public class Employe implements Serializable, Comparable<Employe> {
    private int numero;
    private String nom;
    private String prenom;
    private String tel;
    private String adresse;

    /**
     * Constructor
     * @param numero - Unique employee number
     * @param nom - Surname of the employee
     * @param prenom - First name of the employee
     * @param tel - Telephone number of the employee
     * @param adresse - Address of the employee
     */
    public Employe(int numero, String nom, String prenom, String tel, String adresse) {
        this.numero = numero;
        this.nom = nom;
        this.prenom = prenom;
        this.tel = tel;
        this.adresse = adresse;
    }

    /**
     * Constructor
     * @param numero - Unique employee number
     * @param nom - Surname of the employee
     * @param prenom - First name of the employee
     * @param tel - Telephone number of the employee
     */
    public Employe(int numero, String nom, String prenom, String tel) {
        this.numero = numero;
        this.nom = nom;
        this.prenom = prenom;
        this.tel = tel;
    }

    /**
     * Getter for numero
     * @return - numero
     */
    public int getNumero() {
        return numero;
    }

    /**
     * Setter for numero
     * @param numero - numero
     */
    public void setNumero(int numero) {
        this.numero = numero;
    }

    /**
     * Getter for nom
     * @return - nom
     */
    public String getNom() {
        return nom;
    }

    /**
     * Setter for nom
     * @param nom - nom
     */
    public void setNom(String nom) {
        this.nom = nom;
    }

    /**
     * Getter for prenom
     * @return - prenom
     */
    public String getPrenom() {
        return prenom;
    }

    /**
     * Setter for prenom
     * @param prenom - prenom
     */
    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    /**
     * Getter for tel
     * @return - tel
     */
    public String getTel() {
        return tel;
    }

    /**
     * Setter for tel
     * @param tel -tel
     */
    public void setTel(String tel) {
        this.tel = tel;
    }

    /**
     * Getter for adresse
     * @return - adresse
     */
    public String getAdresse() {
        return adresse;
    }

    /**
     * Setter for adresse
     * @param adresse - adresse
     */
    public void setAdresse(String adresse) {
        this.adresse = adresse;
    }

    @Override
    public String toString() {
        return "Employe{" +
                "numero=" + numero +
                ", nom='" + nom + '\'' +
                ", prenom='" + prenom + '\'' +
                ", tel='" + tel + '\'' +
                ", adresse='" + adresse + '\'' +
                '}';
    }

    @Override
    public int compareTo(Employe o) {
        return Integer.compare(this.numero, o.numero);
    }

    /**
     * Getter for all column names
     * @return - List of column names
     */
    public static String[] getColumnNames() {
        return new String[]{"numero", "nom", "prenom", "adresse", "tel"};
    }

    /**
     * Universal getter
     * @param columnName - name of the field
     * @return Object
     */
    public Object getFieldUniversally(String columnName) {
        switch (columnName) {
            case "numero": return this.numero;
            case "nom": return this.nom;
            case "prenom": return this.prenom;
            case "adresse": return this.adresse;
            case "tel": return this.tel;
            default: return this;
        }
    }

    /**
     * Universal setter
     * @param columnName - name of the field
     * @param value - new value
     */
    public void setFieldUniversally(String columnName, Object value) {
        switch (columnName) {
            case "numero": this.numero = (int) value;
            case "nom": this.nom = (String) value;
            case "prenom": this.prenom = (String) value;
            case "adresse": this.adresse = (String) value;
            case "tel": this.tel = (String) value;
        }
    }
}
