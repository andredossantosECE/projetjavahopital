package Modele;

import java.io.Serializable;

/**
 * Class Service
 */
public class Service implements Serializable {
    private String code;
    private String nom;
    private String batiment;
    private int directeur;

    /**
     * Constructor
     * @param code - Service code
     * @param nom - Name of the service
     * @param batiment - Service's building
     * @param directeur - Director of the service
     */
    public Service(String code, String nom, String batiment, int directeur) {
        this.code = code;
        this.nom = nom;
        this.batiment = batiment;
        this.directeur = directeur;
    }

    /**
     * Getter for code
     * @return - code
     */
    public String getCode() {
        return code;
    }

    /**
     * Setter for code
     * @param code - code
     */
    public void setCode(String code) {
        this.code = code;
    }

    /**
     * Getter for nom
     * @return - nom
     */
    public String getNom() {
        return nom;
    }

    /**
     * Setter for nom
     * @param nom - nom
     */
    public void setNom(String nom) {
        this.nom = nom;
    }

    /**
     * Getter for batiment
     * @return - batiment
     */
    public String getBatiment() {
        return batiment;
    }

    /**
     * Setter for batiment
     * @param batiment - batiment
     */
    public void setBatiment(String batiment) {
        this.batiment = batiment;
    }

    /**
     * Getter for directeur
     * @return - directeur
     */
    public int getDirecteur() {
        return directeur;
    }

    /**
     * Setter for directeur
     * @param directeur - directeur
     */
    public void setDirecteur(int directeur) {
        this.directeur = directeur;
    }

    @Override
    public String toString() {
        return "Service{" +
                "code=" + code +
                ", nom='" + nom + '\'' +
                ", batiment='" + batiment + '\'' +
                ", directeur=" + directeur +
                '}';
    }

    /**
     * Getter for all column names
     * @return - List of column names
     */
    public static String[] getColumnNames() {
        return new String[]{"code", "nom", "batiment", "directeur"};
    }

    /**
     * Universal getter
     * @param columnName - name of the field
     * @return Object
     */
    public Object getFieldUniversally(String columnName) {
        switch (columnName) {
            case "code": return this.code;
            case "nom": return this.nom;
            case "batiment": return this.batiment;
            case "directeur": return this.directeur;
            default: return this;
        }
    }

    /**
     * Universal setter
     * @param columnName - name of the field
     * @param value - new value
     */
    public void setFieldUniversally(String columnName, Object value) {
        switch (columnName) {
            case "code": this.code = (String) value;
            case "nom": this.nom = (String) value;
            case "batiment": this.batiment = (String) value;
            case "directeur": this.directeur = (int) value;
        }
    }
}
