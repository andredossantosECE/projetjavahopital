package Modele;

import java.io.Serializable;

/**
 * Class Docteur (Doctor)
 */
public class Docteur implements Serializable, Comparable<Docteur> {
    private int numero;
    private String specialite;

    /**
     * Constructor
     * @param numero - Number of the doctor
     * @param specialite - Specialty of the doctor
     */
    public Docteur(int numero, String specialite) {
        this.numero = numero;
        this.specialite = specialite;
    }

    /**
     * Constructor
     * @param numero - Number of the doctor
     */
    public Docteur(int numero) {
        this.numero = numero;
    }

    /**
     * Getter for numero
     * @return - numero
     */
    public int getNumero() {
        return numero;
    }

    /**
     * Setter for numero
     * @param numero - numero
     */
    public void setNumero(int numero) {
        this.numero = numero;
    }

    /**
     * Getter for specialite
     * @return - specialite
     */
    public String getSpecialite() {
        return specialite;
    }

    /**
     * Setter for specialite
     * @param specialite - specialite
     */
    public void setSpecialite(String specialite) {
        this.specialite = specialite;
    }

    @Override
    public String toString() {
        return "Docteur{" +
                "numero=" + numero +
                ", specialite='" + specialite + '\'' +
                '}';
    }

    @Override
    public int compareTo(Docteur o) {
        return Integer.compare(this.numero, o.numero);
    }

    /**
     * Getter for all column names
     * @return - List of column names
     */
    public static String[] getColumnNames() {
        return new String[]{"numero", "specialite"};
    }

    /**
     * Universal getter
     * @param columnName - name of the field
     * @return Object
     */
    public Object getFieldUniversally(String columnName) {
        switch (columnName) {
            case "numero": return this.numero;
            case "specialite": return this.specialite;
            default: return this;
        }
    }

    /**
     * Universal setter
     * @param columnName - name of the field
     * @param value - new value
     */
    public void setFieldUniversally(String columnName, Object value) {
        switch (columnName) {
            case "numero": this.numero = (int) value;
            case "specialite": this.specialite = (String) value;
        }
    }
}
