package Modele;

import java.io.Serializable;

/**
 * Class Soigne (Treat)
 */
public class Soigne implements Serializable {
    private int no_docteur;
    private int no_malade;

    /**
     * Constructor
     * @param no_docteur - Doctor's employee number
     * @param no_malade - Patient's number
     */
    public Soigne(int no_docteur, int no_malade) {
        this.no_docteur = no_docteur;
        this.no_malade = no_malade;
    }

    /**
     * Getter for no_docteur
     * @return - no_docteur
     */
    public int getNo_docteur() {
        return no_docteur;
    }

    /**
     * Setter for no_docteur
     * @param no_docteur - no_docteur
     */
    public void setNo_docteur(int no_docteur) {
        this.no_docteur = no_docteur;
    }

    /**
     * Getter for no_malade
     * @return - no_malade
     */
    public int getNo_malade() {
        return no_malade;
    }

    /**
     * Setter for no_malade
     * @param no_malade - no_malade
     */
    public void setNo_malade(int no_malade) {
        this.no_malade = no_malade;
    }

    @Override
    public String toString() {
        return "Soigne{" +
                "no_docteur=" + no_docteur +
                ", no_malade=" + no_malade +
                '}';
    }

    /**
     * Getter for all column names
     * @return - List of column names
     */
    public static String[] getColumnNames() {
        return new String[]{"no_docteur", "no_malade"};
    }

    /**
     * Universal getter
     * @param columnName - name of the field
     * @return - Object
     */
    public Object getFieldUniversally(String columnName) {
        switch (columnName) {
            case "no_docteur": return this.no_docteur;
            case "no_malade": return this.no_malade;
            default: return this;
        }
    }

    /**
     * Universal setter
     * @param columnName - name of the field
     * @param value - new value
     */
    public void setFieldUniversally(String columnName, Object value) {
        switch (columnName) {
            case "no_docteur": this.no_docteur = (int) value;
            case "no_malade": this.no_malade = (int) value;
        }
    }
}
