package Modele;

import java.io.Serializable;

/**
 * Class Hospitalisation
 */
public class Hospitalisation implements Serializable, Comparable<Hospitalisation> {
    private int no_malade;
    private String code_service;
    private int no_chambre;
    private int lit;

    /**
     * Constructor
     * @param no_malade - Patient's number
     * @param code_service - Service code
     * @param no_chambre - Number of the bedroom
     * @param lit - Number of the bed
     */
    public Hospitalisation(int no_malade, String code_service, int no_chambre, int lit) {
        this.no_malade = no_malade;
        this.code_service = code_service;
        this.no_chambre = no_chambre;
        this.lit = lit;
    }

    /**
     * Getter for no_malade
     * @return - no_malade
     */
    public int getNo_malade() {
        return no_malade;
    }

    /**
     * Setter for no_malade
     * @param no_malade - no_malade
     */
    public void setNo_malade(int no_malade) {
        this.no_malade = no_malade;
    }

    /**
     * Getter for code_service
     * @return - code_service
     */
    public String getCode_service() {
        return code_service;
    }

    /**
     * Setter for code_service
     * @param code_service - code_service
     */
    public void setCode_service(String code_service) {
        this.code_service = code_service;
    }

    /**
     * Getter for no_chambre
     * @return - no_chambre
     */
    public int getNo_chambre() {
        return no_chambre;
    }

    /**
     * Setter for no_chambre
     * @param no_chambre - no_chambre
     */
    public void setNo_chambre(int no_chambre) {
        this.no_chambre = no_chambre;
    }

    /**
     * Getter for lit
     * @return - lit
     */
    public int getLit() {
        return lit;
    }

    /**
     * Setter for lit
     * @param lit - lit
     */
    public void setLit(int lit) {
        this.lit = lit;
    }

    @Override
    public String toString() {
        return "Hospitalisation{" +
                "no_malade=" + no_malade +
                ", code_service=" + code_service +
                ", no_chambre=" + no_chambre +
                ", lit=" + lit +
                '}';
    }

    @Override
    public int compareTo(Hospitalisation o) {
        return Integer.compare(this.no_malade, o.no_malade);
    }

    /**
     * Getter for all column names
     * @return - List of column names
     */
    public static String[] getColumnNames() {
        return new String[]{"no_malade", "code_service", "no_chambre", "lit"};
    }

    /**
     * Universal getter
     * @param columnName - name of the field
     * @return Object
     */
    public Object getFieldUniversally(String columnName) {
        switch (columnName) {
            case "no_malade": return this.no_malade;
            case "code_service": return this.code_service;
            case "no_chambre": return this.no_chambre;
            case "lit": return this.lit;
            default: return this;
        }
    }

    /**
     * Universal setter
     * @param columnName - name of the field
     * @param value - new value
     */
    public void setFieldUniversally(String columnName, Object value) {
        switch (columnName) {
            case "no_malade": this.no_malade = (int) value;
            case "code_service": this.code_service = (String) value;
            case "no_chambre": this.no_chambre = (int) value;
            case "lit": this.lit = (int) value;
        }
    }
}
