package Modele;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Class Chambre (Bedroom)
 */
public class Chambre implements Serializable, Comparable<Chambre> {
    private int no_chambre;
    private String code_service;
    private int surveillant;
    private int nb_lits;

    /**
     * Constructor
     * @param no_chambre - Bedroom Number
     * @param code_service - Service code for this bedroom
     * @param surveillant - Guardian (Doctor/Nurse) of this room
     * @param nb_lits - The amount of beds on this room
     */
    public Chambre(int no_chambre, String code_service, int surveillant, int nb_lits) {
        this.no_chambre = no_chambre;
        this.code_service = code_service;
        this.surveillant = surveillant;
        this.nb_lits = nb_lits;
    }

    /**
     * Getter for no_chambre
     * @return - no_chambre
     */
    public int getNo_chambre() {
        return no_chambre;
    }

    /**
     * Setter for no_chambre
     * @param no_chambre - no_chambre
     */
    public void setNo_chambre(int no_chambre) {
        this.no_chambre = no_chambre;
    }

    /**
     * Getter for code_service
     * @return - code_service
     */
    public String getCode_service() {
        return code_service;
    }

    /**
     * Setter for code_service
     * @param code_service - code_service
     */
    public void setCode_service(String code_service) {
        this.code_service = code_service;
    }

    /**
     * Getter for surveillant
     * @return - surveillant
     */
    public int getSurveillant() {
        return surveillant;
    }

    /**
     * Setter for surveillant
     * @param surveillant - surveillant
     */
    public void setSurveillant(int surveillant) {
        this.surveillant = surveillant;
    }

    /**
     * Getter for nb_lits
     * @return - nb_lits
     */
    public int getNb_lits() {
        return nb_lits;
    }

    /**
     * Setter for nb_lits
     * @param nb_lits - nb_lits
     */
    public void setNb_lits(int nb_lits) {
        this.nb_lits = nb_lits;
    }

    @Override
    public String toString() {
        return "Chambre{" +
                "no_chambre=" + no_chambre +
                ", code_service=" + code_service +
                ", surveillant=" + surveillant +
                ", nb_lits=" + nb_lits +
                '}';
    }

    @Override
    public int compareTo(Chambre o) {
        return Integer.compare(this.no_chambre, o.no_chambre);
    }

    /**
     * Getter for all column names
     * @return - List of column names
     */
    public static String[] getColumnNames() {
        return new String[]{"code_service", "no_chambre", "surveillant", "nb_lits"};
    }

    /**
     * Universal getter
     * @param columnName - name of the field
     * @return Object
     */
    public Object getFieldUniversally(String columnName) {
        switch (columnName) {
            case "code_service": return this.code_service;
            case "no_chambre": return this.no_chambre;
            case "surveillant": return this.surveillant;
            case "nb_lits": return this.nb_lits;
            default: return this;
        }
    }

    /**
     * Universal setter
     * @param columnName - name of the field
     * @param value - new value
     */
    public void setFieldUniversally(String columnName, Object value) {
        switch (columnName) {
            case "code_service": this.code_service = (String) value;
            case "no_chambre": this.no_chambre = (int) value;
            case "surveillant": this.surveillant = (int) value;
            case "nb_lits": this.nb_lits = (int) value;
        }
    }
}
