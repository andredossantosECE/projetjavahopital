package Modele;

import java.io.Serializable;

/**
 * Class Infirmier (nurse)
 */
public class Infirmier implements Serializable, Comparable<Infirmier> {
    private int numero;
    private String code_service;
    private String rotation;
    private float salaire;

    /**
     * Constructor
     * @param numero - Nurse's employee number
     * @param code_service - Service code of the nurse
     * @param rotation - Rotation (Jour/Nuit)
     * @param salaire - Salary of the nurse
     */
    public Infirmier(int numero, String code_service, String rotation, float salaire) {
        this.numero = numero;
        this.code_service = code_service;
        this.rotation = rotation;
        this.salaire = salaire;
    }

    /**
     * Constructor
     * @param numero - Nurse's employee number
     * @param rotation - Rotation (Jour/Nuit)
     */
    public Infirmier(int numero, String rotation) {
        this.numero = numero;
        this.rotation = rotation;
    }

    /**
     * Getter for numero
     * @return - numero
     */
    public int getNumero() {
        return numero;
    }

    /**
     * Setter for numero
     * @param numero - numero
     */
    public void setNumero(int numero) {
        this.numero = numero;
    }

    /**
     * Getter for code_service
     * @return - code_service
     */
    public String getCode_service() {
        return code_service;
    }

    /**
     * Setter for code_service
     * @param code_service - code_service
     */
    public void setCode_service(String code_service) {
        this.code_service = code_service;
    }

    /**
     * Getter for rotation
     * @return - rotation
     */
    public String getRotation() {
        return rotation;
    }

    /**
     * Setter for rotation
     * @param rotation - rotation
     */
    public void setRotation(String rotation) {
        this.rotation = rotation;
    }

    /**
     * Getter for salaire
     * @return - salaire
     */
    public float getSalaire() {
        return salaire;
    }

    /**
     * Setter for salaire
     * @param salaire - salaire
     */
    public void setSalaire(float salaire) {
        this.salaire = salaire;
    }

    @Override
    public String toString() {
        return "Infirmier{" +
                "numero=" + numero +
                ", code_service=" + code_service +
                ", rotation=" + rotation +
                ", salaire=" + salaire +
                '}';
    }

    @Override
    public int compareTo(Infirmier o) {
        return Integer.compare(this.numero, o.numero);
    }

    /**
     * Getter for all column names
     * @return - List of column names
     */
    public static String[] getColumnNames() {
        return new String[]{"numero", "code_service", "rotation", "salaire"};
    }

    /**
     * Universal getter
     * @param columnName - name of the field
     * @return Object
     */
    public Object getFieldUniversally(String columnName) {
        switch (columnName) {
            case "numero": return this.numero;
            case "code_service": return this.code_service;
            case "rotation": return this.rotation;
            case "salaire": return this.salaire;
            default: return this;
        }
    }

    /**
     * Universal setter
     * @param columnName - name of the field
     * @param value - new value
     */
    public void setFieldUniversally(String columnName, Object value) {
        switch (columnName) {
            case "numero": this.numero = (int) value;
            case "code_service": this.code_service = (String) value;
            case "rotation": this.rotation = (String) value;
            case "salaire": this.salaire = (int) value;
        }
    }
}
