package Modele;

import java.io.Serializable;

/**
 * Class Malade (Patient)
 */
public class Malade implements Serializable, Comparable<Malade> {
    private int numero;
    private String nom;
    private String prenom;
    private String tel;
    private String mutuelle;
    private String adresse;

    /**
     * Constructor
     * @param numero - Patient's number
     * @param nom - Surname
     * @param prenom - First name
     * @param adresse - Address
     * @param tel - Telephone number
     * @param mutuelle - Health Care insurance
     */
    public Malade(int numero, String nom, String prenom, String adresse, String tel, String mutuelle) {
        this.numero = numero;
        this.nom = nom;
        this.prenom = prenom;
        this.adresse = adresse;
        this.tel = tel;
        this.mutuelle = mutuelle;
    }

    /**
     * Getter for numero
     * @return - numero
     */
    public int getNumero() {
        return numero;
    }

    /**
     * Setter for numero
     * @param numero - numero
     */
    public void setNumero(int numero) {
        this.numero = numero;
    }

    /**
     * Getter for nom
     * @return - nom
     */
    public String getNom() {
        return nom;
    }

    /**
     * Setter for nom
     * @param nom - nom
     */
    public void setNom(String nom) {
        this.nom = nom;
    }

    /**
     * Getter for prenom
     * @return - prenom
     */
    public String getPrenom() {
        return prenom;
    }

    /**
     * Setter for prenom
     * @param prenom - prenom
     */
    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    /**
     * Getter for adresse
     * @return - adresse
     */
    public String getAdresse() {
        return adresse;
    }

    /**
     * Setter for adresse
     * @param adresse - adresse
     */
    public void setAdresse(String adresse) {
        this.adresse = adresse;
    }

    /**
     * Getter for tel
     * @return - tel
     */
    public String getTel() {
        return tel;
    }

    /**
     * Setter for tel
     * @param tel - tel
     */
    public void setTel(String tel) {
        this.tel = tel;
    }

    /**
     * Getter for mutuelle
     * @return - mutuelle
     */
    public String getMutuelle() {
        return mutuelle;
    }

    /**
     * Setter for mutuelle
     * @param mutuelle - mutuelle
     */
    public void setMutuelle(String mutuelle) {
        this.mutuelle = mutuelle;
    }

    @Override
    public String toString() {
        return "Malade{" +
                "numero=" + numero +
                ", nom='" + nom + '\'' +
                ", prenom='" + prenom + '\'' +
                ", adresse='" + adresse + '\'' +
                ", tel='" + tel + '\'' +
                ", mutuelle='" + mutuelle + '\'' +
                '}';
    }

    @Override
    public int compareTo(Malade o) {
        return Integer.compare(this.numero, o.numero);
    }

    /**
     * Getter for all column names
     * @return - List of column names
     */
    public static String[] getColumnNames() {
        return new String[]{"numero", "nom", "prenom", "adresse", "tel", "mutuelle"};
    }

    /**
     * Universal getter
     * @param columnName - name of the field
     * @return Object
     */
    public Object getFieldUniversally(String columnName) {
        switch (columnName) {
            case "numero": return this.numero;
            case "nom": return this.nom;
            case "prenom": return this.prenom;
            case "adresse": return this.adresse;
            case "tel": return this.tel;
            case "mutuelle": return this.mutuelle;
            default: return this;
        }
    }

    /**
     * Universal setter
     * @param columnName - name of the field
     * @param value - new value
     */
    public void setFieldUniversally(String columnName, Object value) {
        switch (columnName) {
            case "numero": this.numero = (int) value;
            case "nom": this.nom = (String) value;
            case "prenom": this.prenom = (String) value;
            case "adresse": this.adresse = (String) value;
            case "tel": this.tel = (String) value;
            case "mutuelle": this.mutuelle = (String) value;
        }
    }
}
