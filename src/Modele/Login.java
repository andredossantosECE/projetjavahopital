package Modele;

import java.io.Serializable;

/**
 * Class Login
 */
public class Login implements Serializable, Comparable<Login> {
    private int idLogin;
    private String username;
    private String password;
    private int employe;
    private String fonction;
    private boolean validated = false;

    /**
     * Constructor
     * @param idLogin - id of the user
     * @param username - username
     * @param password - password
     * @param employe - Employee number
     * @param fonction - User's role
     * @param validated - Login validation
     */
    public Login(int idLogin, String username, String password, int employe, String fonction, boolean validated) {
        this.idLogin = idLogin;
        this.username = username;
        this.password = password;
        this.employe = employe;
        this.fonction = fonction;
        this.validated = validated;
    }

    /**
     * Constructor
     * @param idLogin - id of the user
     * @param username - username
     * @param password - password
     * @param employe - Employee number
     * @param fonction - User's role
     */
    public Login(int idLogin, String username, String password, int employe, String fonction) {
        this.idLogin = idLogin;
        this.username = username;
        this.password = password;
        this.employe = employe;
        this.fonction = fonction;
    }

    /**
     * Constructor
     * @param idLogin - id of the user
     * @param username - username
     * @param password - password
     * @param employe - Employee number
     */
    public Login(int idLogin, String username, String password, int employe) {
        this.idLogin = idLogin;
        this.username = username;
        this.password = password;
        this.employe = employe;
    }

    /**
     * Constructor
     * @param idLogin - id of the user
     * @param username - username
     * @param password - password
     */
    public Login(int idLogin, String username, String password) {
        this.idLogin = idLogin;
        this.username = username;
        this.password = password;
    }

    /**
     * Getter for idLogin
     * @return - idLogin
     */
    public int getIdLogin() {
        return idLogin;
    }

    /**
     * Setter for idLogin
     * @param idLogin - idLogin
     */
    public void setIdLogin(int idLogin) {
        this.idLogin = idLogin;
    }

    /**
     * Getter for username
     * @return - username
     */
    public String getUsername() {
        return username;
    }

    /**
     * Setter for username
     * @param username - username
     */
    public void setUsername(String username) {
        this.username = username;
    }

    /**
     * Getter for password
     * @return - password
     */
    public String getPassword() {
        return password;
    }

    /**
     * Setter for password
     * @param password - password
     */
    public void setPassword(String password) {
        this.password = password;
    }

    /**
     * Getter for employe
     * @return - employe
     */
    public int getEmploye() {
        return employe;
    }

    /**
     * Setter for employe
     * @param employe - employe
     */
    public void setEmploye(int employe) {
        this.employe = employe;
    }

    /**
     * Getter for fonction
     * @return - fonction
     */
    public String getFonction() {
        return fonction;
    }

    /**
     * Setter for fonction
     * @param fonction - fonction
     */
    public void setFonction(String fonction) {
        this.fonction = fonction;
    }

    /**
     * Getter for validated
     * @return - validated
     */
    public boolean isValidated() {
        return validated;
    }

    /**
     * Setter for validated
     * @param validated - validated
     */
    public void setValidated(boolean validated) {
        this.validated = validated;
    }

    @Override
    public String toString() {
        return "Login{" +
                "idLogin=" + idLogin +
                ", username='" + username + '\'' +
                ", password='" + password + '\'' +
                ", employe=" + employe +
                ", fonction='" + fonction + '\'' +
                ", validated=" + validated +
                '}';
    }

    @Override
    public int compareTo(Login o) {
        return Integer.compare(this.idLogin, o.idLogin);
    }

    /**
     * Getter for all column names
     * @return - List of column names
     */
    public static String[] getColumnNames() {
        return new String[]{"idLogin", "username", "password", "employe", "fonction", "validated"};
    }

    /**
     * Universal getter
     * @param columnName - name of the field
     * @return Object
     */
    public Object getFieldUniversally(String columnName) {
        switch (columnName) {
            case "idLogin": return this.idLogin;
            case "username": return this.username;
            case "password": return this.password;
            case "employe": return this.employe;
            case "fonction": return this.fonction;
            case "validated": return this.validated;
            default: return this;
        }
    }

    /**
     * Universal setter
     * @param columnName - name of the field
     * @param value - new value
     */
    public void setFieldUniversally(String columnName, Object value) {
        switch (columnName) {
            case "idLogin": this.idLogin = (int) value;
            case "username": this.username = (String) value;
            case "password": this.password = (String) value;
            case "employe": this.employe = (int) value;
            case "fonction": this.fonction = (String) value;
            case "validated": this.validated = (boolean) value;
        }
    }
}
