create table if not exists Employe
(
    numero  int auto_increment
        primary key,
    nom     varchar(45)  not null,
    prenom  varchar(45)  not null,
    tel     varchar(45)  not null,
    adresse varchar(100) not null,
    constraint Employe_AssossKey
        unique (nom, prenom, tel)
);

create table if not exists Docteur
(
    numero     int auto_increment
        primary key,
    specialite varchar(45) null,
    constraint FK_numero_employe
        foreign key (numero) references Employe (numero)
);

create table if not exists Login
(
    idLogin  int auto_increment
        primary key,
    username varchar(45) not null,
    password varchar(45) not null,
    employe  int         null,
    fonction varchar(10) null,
    constraint username_UNIQUE
        unique (username),
    constraint FK_login_employee
        foreign key (employe) references Employe (numero)
);

create index FK_login_employee_idx
    on Login (employe);

create table if not exists Malade
(
    numero   int auto_increment
        primary key,
    nom      varchar(45)  not null,
    prenom   varchar(45)  not null,
    tel      varchar(45)  not null,
    mutuelle varchar(45)  null,
    adresse  varchar(100) null,
    constraint Malade_AssossKey
        unique (nom, prenom, tel)
);

create table if not exists Service
(
    code      varchar(10) not null,
    nom       varchar(45) not null,
    batiment  varchar(30) not null,
    directeur int         not null,
    constraint numero_UNIQUE
        unique (code),
    constraint FK_directeur
        foreign key (directeur) references Docteur (numero)
);

create index FK_directeur_idx
    on Service (directeur);

alter table Service
    add primary key (code);

create table if not exists Infirmier
(
    numero       int auto_increment
        primary key,
    code_service varchar(10) not null,
    rotation     varchar(30) null,
    salaire      float       null,
    constraint FK2_numero_employe
        foreign key (numero) references Employe (numero),
    constraint FK_code_service
        foreign key (code_service) references Service (code)
);

create table if not exists Chambre
(
    no_chambre   int auto_increment,
    code_service varchar(10) not null,
    surveillant  int         null,
    nb_lits      int         null,
    primary key (no_chambre, code_service),
    constraint FK3_code_service
        foreign key (code_service) references Service (code),
    constraint FK_surveillant
        foreign key (surveillant) references Infirmier (numero)
);

create index FK3_code_service_idx
    on Chambre (code_service);

create index FK_surveillant_idx
    on Chambre (surveillant);

create table if not exists Hospitalisation
(
    no_malade    int auto_increment
        primary key,
    code_service varchar(10) not null,
    no_chambre   int         not null,
    lit          int         not null,
    constraint FK4_code_service
        foreign key (code_service) references Service (code),
    constraint FK_no_chambre
        foreign key (no_chambre) references Chambre (no_chambre),
    constraint FK_no_malade
        foreign key (no_malade) references Malade (numero)
);

create index FK4_code_service_idx
    on Hospitalisation (code_service);

create index FK_no_chambre_idx
    on Hospitalisation (no_chambre);

create index FK_code_service_idx
    on Infirmier (code_service);

create table if not exists Soigne
(
    no_docteur int not null,
    no_malade  int not null,
    primary key (no_docteur, no_malade),
    constraint FK2_no_malade
        foreign key (no_malade) references Malade (numero),
    constraint FK_no_docteur
        foreign key (no_docteur) references Docteur (numero)
);

create index FK2_no_malade_idx
    on Soigne (no_malade);


