-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Jan 12, 2020 at 12:46 AM
-- Server version: 8.0.13-4
-- PHP Version: 7.2.24-0ubuntu0.18.04.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `kAhTna6s5C`
--

-- --------------------------------------------------------

--
-- Table structure for table `Chambre`
--

CREATE TABLE `Chambre` (
  `no_chambre` int(11) NOT NULL,
  `code_service` varchar(10) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `surveillant` int(11) DEFAULT NULL,
  `nb_lits` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `Docteur`
--

CREATE TABLE `Docteur` (
  `numero` int(11) NOT NULL,
  `specialite` varchar(45) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `Employe`
--

CREATE TABLE `Employe` (
  `numero` int(11) NOT NULL,
  `nom` varchar(45) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `prenom` varchar(45) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `tel` varchar(45) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `adresse` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `Hospitalisation`
--

CREATE TABLE `Hospitalisation` (
  `no_malade` int(11) NOT NULL,
  `code_service` varchar(10) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `no_chambre` int(11) NOT NULL,
  `lit` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `Infirmier`
--

CREATE TABLE `Infirmier` (
  `numero` int(11) NOT NULL,
  `code_service` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `rotation` varchar(30) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `salaire` float DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `Login`
--

CREATE TABLE `Login` (
  `idLogin` int(11) NOT NULL,
  `username` varchar(45) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(45) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `employe` int(11) DEFAULT NULL,
  `fonction` varchar(10) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `validated` tinyint(1) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `Malade`
--

CREATE TABLE `Malade` (
  `numero` int(11) NOT NULL,
  `nom` varchar(45) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `prenom` varchar(45) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `tel` varchar(45) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `mutuelle` varchar(45) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `adresse` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `Service`
--

CREATE TABLE `Service` (
  `code` varchar(10) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `nom` varchar(45) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `batiment` varchar(30) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `directeur` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `Soigne`
--

CREATE TABLE `Soigne` (
  `no_docteur` int(11) NOT NULL,
  `no_malade` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `Chambre`
--
ALTER TABLE `Chambre`
  ADD PRIMARY KEY (`no_chambre`,`code_service`),
  ADD KEY `FK3_code_service_idx` (`code_service`),
  ADD KEY `FK_surveillant_idx` (`surveillant`);

--
-- Indexes for table `Docteur`
--
ALTER TABLE `Docteur`
  ADD PRIMARY KEY (`numero`);

--
-- Indexes for table `Employe`
--
ALTER TABLE `Employe`
  ADD PRIMARY KEY (`numero`),
  ADD UNIQUE KEY `Employe_AssossKey` (`nom`,`prenom`,`tel`);

--
-- Indexes for table `Hospitalisation`
--
ALTER TABLE `Hospitalisation`
  ADD PRIMARY KEY (`no_malade`),
  ADD KEY `FK4_code_service_idx` (`code_service`),
  ADD KEY `FK_no_chambre_idx` (`no_chambre`);

--
-- Indexes for table `Infirmier`
--
ALTER TABLE `Infirmier`
  ADD PRIMARY KEY (`numero`),
  ADD KEY `FK_code_service_idx` (`code_service`);

--
-- Indexes for table `Login`
--
ALTER TABLE `Login`
  ADD PRIMARY KEY (`idLogin`),
  ADD UNIQUE KEY `username_UNIQUE` (`username`),
  ADD KEY `FK_login_employee_idx` (`employe`);

--
-- Indexes for table `Malade`
--
ALTER TABLE `Malade`
  ADD PRIMARY KEY (`numero`),
  ADD UNIQUE KEY `Malade_AssossKey` (`nom`,`prenom`,`tel`);

--
-- Indexes for table `Service`
--
ALTER TABLE `Service`
  ADD PRIMARY KEY (`code`),
  ADD UNIQUE KEY `numero_UNIQUE` (`code`),
  ADD KEY `FK_directeur_idx` (`directeur`);

--
-- Indexes for table `Soigne`
--
ALTER TABLE `Soigne`
  ADD PRIMARY KEY (`no_docteur`,`no_malade`),
  ADD KEY `FK2_no_malade_idx` (`no_malade`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `Chambre`
--
ALTER TABLE `Chambre`
  MODIFY `no_chambre` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `Docteur`
--
ALTER TABLE `Docteur`
  MODIFY `numero` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `Employe`
--
ALTER TABLE `Employe`
  MODIFY `numero` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `Hospitalisation`
--
ALTER TABLE `Hospitalisation`
  MODIFY `no_malade` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `Infirmier`
--
ALTER TABLE `Infirmier`
  MODIFY `numero` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `Login`
--
ALTER TABLE `Login`
  MODIFY `idLogin` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `Malade`
--
ALTER TABLE `Malade`
  MODIFY `numero` int(11) NOT NULL AUTO_INCREMENT;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `Chambre`
--
ALTER TABLE `Chambre`
  ADD CONSTRAINT `FK3_code_service` FOREIGN KEY (`code_service`) REFERENCES `Service` (`code`),
  ADD CONSTRAINT `FK_surveillant` FOREIGN KEY (`surveillant`) REFERENCES `Infirmier` (`numero`);

--
-- Constraints for table `Docteur`
--
ALTER TABLE `Docteur`
  ADD CONSTRAINT `FK_numero_employe` FOREIGN KEY (`numero`) REFERENCES `Employe` (`numero`);

--
-- Constraints for table `Hospitalisation`
--
ALTER TABLE `Hospitalisation`
  ADD CONSTRAINT `FK4_code_service` FOREIGN KEY (`code_service`) REFERENCES `Service` (`code`),
  ADD CONSTRAINT `FK_no_chambre` FOREIGN KEY (`no_chambre`) REFERENCES `Chambre` (`no_chambre`),
  ADD CONSTRAINT `FK_no_malade` FOREIGN KEY (`no_malade`) REFERENCES `Malade` (`numero`);

--
-- Constraints for table `Infirmier`
--
ALTER TABLE `Infirmier`
  ADD CONSTRAINT `FK2_numero_employe` FOREIGN KEY (`numero`) REFERENCES `Employe` (`numero`),
  ADD CONSTRAINT `FK_code_service` FOREIGN KEY (`code_service`) REFERENCES `Service` (`code`);

--
-- Constraints for table `Login`
--
ALTER TABLE `Login`
  ADD CONSTRAINT `FK_login_employee` FOREIGN KEY (`employe`) REFERENCES `Employe` (`numero`);

--
-- Constraints for table `Service`
--
ALTER TABLE `Service`
  ADD CONSTRAINT `FK_directeur` FOREIGN KEY (`directeur`) REFERENCES `Docteur` (`numero`);

--
-- Constraints for table `Soigne`
--
ALTER TABLE `Soigne`
  ADD CONSTRAINT `FK2_no_malade` FOREIGN KEY (`no_malade`) REFERENCES `Malade` (`numero`),
  ADD CONSTRAINT `FK_no_docteur` FOREIGN KEY (`no_docteur`) REFERENCES `Docteur` (`numero`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
