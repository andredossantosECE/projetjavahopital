create table Soigne
(
	no_docteur int not null,
	no_malade int not null,
	primary key (no_docteur, no_malade),
	constraint FK2_no_malade
		foreign key (no_malade) references Malade (numero),
	constraint FK_no_docteur
		foreign key (no_docteur) references Docteur (numero)
);

create index FK2_no_malade_idx
	on Soigne (no_malade);

