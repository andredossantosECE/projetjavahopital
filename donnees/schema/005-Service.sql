create table Service
(
	code varchar(10) not null,
	nom varchar(45) not null,
	batiment varchar(30) not null,
	directeur int not null,
	constraint numero_UNIQUE
		unique (code),
	constraint FK_directeur
		foreign key (directeur) references Docteur (numero)
);

create index FK_directeur_idx
	on Service (directeur);

alter table Service
	add primary key (code);

