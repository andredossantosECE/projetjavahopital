create table Chambre
(
	no_chambre int auto_increment,
	code_service varchar(10) not null,
	surveillant int null,
	nb_lits int null,
	primary key (no_chambre, code_service),
	constraint FK3_code_service
		foreign key (code_service) references Service (code),
	constraint FK_surveillant
		foreign key (surveillant) references Infirmier (numero)
);

create index FK3_code_service_idx
	on Chambre (code_service);

create index FK_surveillant_idx
	on Chambre (surveillant);

