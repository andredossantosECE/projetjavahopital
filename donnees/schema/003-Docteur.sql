create table Docteur
(
	numero int auto_increment
		primary key,
	specialite varchar(45) null,
	constraint FK_numero_employe
		foreign key (numero) references Employe (numero)
);

