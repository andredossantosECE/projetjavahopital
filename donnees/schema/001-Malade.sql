create table Malade
(
	numero int auto_increment
		primary key,
	nom varchar(45) not null,
	prenom varchar(45) not null,
	tel varchar(45) not null,
	mutuelle varchar(45) null,
	adresse varchar(100) null,
	constraint Malade_AssossKey
		unique (nom, prenom, tel)
);

