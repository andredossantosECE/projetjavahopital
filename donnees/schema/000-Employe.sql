create table Employe
(
	numero int auto_increment
		primary key,
	nom varchar(45) not null,
	prenom varchar(45) not null,
	tel varchar(45) not null,
	adresse varchar(100) null,
	constraint Employe_AssossKey
		unique (nom, prenom, tel)
);

