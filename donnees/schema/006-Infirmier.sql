create table Infirmier
(
	numero int auto_increment
		primary key,
	code_service varchar(10) null,
	rotation varchar(30) null,
	salaire float null,
	constraint FK2_numero_employe
		foreign key (numero) references Employe (numero),
	constraint FK_code_service
		foreign key (code_service) references Service (code)
);

create index FK_code_service_idx
	on Infirmier (code_service);

