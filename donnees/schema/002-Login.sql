create table Login
(
	idLogin int auto_increment
		primary key,
	username varchar(45) not null,
	password varchar(45) not null,
	employe int null,
	fonction varchar(10) null,
	constraint username_UNIQUE
		unique (username),
	constraint FK_login_employee
		foreign key (employe) references Employe (numero)
);

create index FK_login_employee_idx
	on Login (employe);

