create table Hospitalisation
(
	no_malade int auto_increment
		primary key,
	code_service varchar(10) not null,
	no_chambre int not null,
	lit int not null,
	constraint FK4_code_service
		foreign key (code_service) references Service (code),
	constraint FK_no_chambre
		foreign key (no_chambre) references Chambre (no_chambre),
	constraint FK_no_malade
		foreign key (no_malade) references Malade (numero)
);

create index FK4_code_service_idx
	on Hospitalisation (code_service);

create index FK_no_chambre_idx
	on Hospitalisation (no_chambre);

